
#ifndef ERRNO_H
#define ERRNO_H

// Some of the functionality described on this reference page extends the ISO
// C standard. Any conflict between the  requirements  described here and the
// ISO C standard is unintentional. This volume of POSIX.1‐2008 defers to the
// ISO C standard.

// The ISO C standard only requires the symbols [EDOM], [EILSEQ], and [ERANGE]
// to be defined.

// The  <errno.h> header shall provide a declaration or definition for errno.
// The symbol errno shall expand to a modifiable lvalue of type int.  It is
// unspecified whether errno is a macro or an identifier declared with external
// linkage. If a macro definition is suppressed in order to access an actual
// object, or a program defines an identifier with the name errno, the behavior
// is undefined.

#include <errno/errno.h>
#include <errno/program_invocation_name.h>

// The  <errno.h> header shall define the following macros which shall expand
// to integer constant expressions with type int, distinct positive values
// (except as noted below), and which shall be suitable for use in #if
// preprocessing directives:
#include <errno/EPERM.h>
#include <errno/ENOENT.h>
#include <errno/ESRCH.h>
#include <errno/EINTR.h>
#include <errno/EIO.h>
#include <errno/ENXIO.h>
#include <errno/E2BIG.h>
#include <errno/ENOEXEC.h>
#include <errno/EBADF.h>
#include <errno/ECHILD.h>
#include <errno/EAGAIN.h>
#include <errno/ENOMEM.h>
#include <errno/EACCES.h>
#include <errno/EFAULT.h>
#include <errno/ENOTBLK.h>
#include <errno/EBUSY.h>
#include <errno/EEXIST.h>
#include <errno/EXDEV.h>
#include <errno/ENODEV.h>
#include <errno/ENOTDIR.h>
#include <errno/EISDIR.h>
#include <errno/EINVAL.h>
#include <errno/ENFILE.h>
#include <errno/EMFILE.h>
#include <errno/ENOTTY.h>
#include <errno/ETXTBSY.h>
#include <errno/EFBIG.h>
#include <errno/ENOSPC.h>
#include <errno/ESPIPE.h>
#include <errno/EROFS.h>
#include <errno/EMLINK.h>
#include <errno/EPIPE.h>
#include <errno/EDOM.h>
#include <errno/ERANGE.h>
#include <errno/EDEADLK.h>
#include <errno/ENAMETOOLONG.h>
#include <errno/ENOLCK.h>
#include <errno/ENOSYS.h>
#include <errno/ENOTEMPTY.h>
#include <errno/ELOOP.h>
#include <errno/EWOULDBLOCK.h>
#include <errno/ENOMSG.h>
#include <errno/EIDRM.h>
#include <errno/ECHRNG.h>
#include <errno/EL2NSYNC.h>
#include <errno/EL3HLT.h>
#include <errno/EL3RST.h>
#include <errno/ELNRNG.h>
#include <errno/EUNATCH.h>
#include <errno/ENOCSI.h>
#include <errno/EL2HLT.h>
#include <errno/EBADE.h>
#include <errno/EBADR.h>
#include <errno/EXFULL.h>
#include <errno/ENOANO.h>
#include <errno/EBADRQC.h>
#include <errno/EBADSLT.h>
#include <errno/EDEADLOCK.h>
#include <errno/EBFONT.h>
#include <errno/ENOSTR.h>
#include <errno/ENODATA.h>
#include <errno/ETIME.h>
#include <errno/ENOSR.h>
#include <errno/ENONET.h>
#include <errno/ENOPKG.h>
#include <errno/EREMOTE.h>
#include <errno/ENOLINK.h>
#include <errno/EADV.h>
#include <errno/ESRMNT.h>
#include <errno/ECOMM.h>
#include <errno/EPROTO.h>
#include <errno/EMULTIHOP.h>
#include <errno/EDOTDOT.h>
#include <errno/EBADMSG.h>
#include <errno/EOVERFLOW.h>
#include <errno/ENOTUNIQ.h>
#include <errno/EBADFD.h>
#include <errno/EREMCHG.h>
#include <errno/ELIBACC.h>
#include <errno/ELIBBAD.h>
#include <errno/ELIBSCN.h>
#include <errno/ELIBMAX.h>
#include <errno/ELIBEXEC.h>
#include <errno/EILSEQ.h>
#include <errno/ERESTART.h>
#include <errno/ESTRPIPE.h>
#include <errno/EUSERS.h>
#include <errno/ENOTSOCK.h>
#include <errno/EDESTADDRREQ.h>
#include <errno/EMSGSIZE.h>
#include <errno/EPROTOTYPE.h>
#include <errno/ENOPROTOOPT.h>
#include <errno/EPROTONOSUPPORT.h>
#include <errno/ESOCKTNOSUPPORT.h>
#include <errno/EOPNOTSUPP.h>
#include <errno/EPFNOSUPPORT.h>
#include <errno/EAFNOSUPPORT.h>
#include <errno/EADDRINUSE.h>
#include <errno/EADDRNOTAVAIL.h>
#include <errno/ENETDOWN.h>
#include <errno/ENETUNREACH.h>
#include <errno/ENETRESET.h>
#include <errno/ECONNABORTED.h>
#include <errno/ECONNRESET.h>
#include <errno/ENOBUFS.h>
#include <errno/EISCONN.h>
#include <errno/ENOTCONN.h>
#include <errno/ESHUTDOWN.h>
#include <errno/ETOOMANYREFS.h>
#include <errno/ETIMEDOUT.h>
#include <errno/ECONNREFUSED.h>
#include <errno/EHOSTDOWN.h>
#include <errno/EHOSTUNREACH.h>
#include <errno/EALREADY.h>
#include <errno/EINPROGRESS.h>
#include <errno/ESTALE.h>
#include <errno/EUCLEAN.h>
#include <errno/ENOTNAM.h>
#include <errno/ENAVAIL.h>
#include <errno/EISNAM.h>
#include <errno/EREMOTEIO.h>
#include <errno/EDQUOT.h>
#include <errno/ENOMEDIUM.h>
#include <errno/EMEDIUMTYPE.h>
#include <errno/ECANCELED.h>
#include <errno/ENOKEY.h>
#include <errno/EKEYEXPIRED.h>
#include <errno/EKEYREVOKED.h>
#include <errno/EKEYREJECTED.h>
#include <errno/EOWNERDEAD.h>
#include <errno/ENOTRECOVERABLE.h>
#include <errno/ERFKILL.h>
#include <errno/EHWPOISON.h>
#include <errno/ENOTSUP.h>


#endif
