
prefix ?= /

CC = gcc

CPPFLAGS += -I .

CPPFLAGS += -D ENTER=
CPPFLAGS += -D HERE=
CPPFLAGS += -D EXIT=
CPPFLAGS += -D 'dpv(_)='
CPPFLAGS += -D 'dpvc(_)='
CPPFLAGS += -D 'dpvb(_)='
CPPFLAGS += -D 'dpvs(_)='
CPPFLAGS += -D 'dpvo(_)='
CPPFLAGS += -D 'dpvsn(_, __)='
CPPFLAGS += -D 'ddprintf(...)='
CPPFLAGS += -D TODO='assert(!"TODO")'
CPPFLAGS += -D CHECK='assert(!"CHECK")'

CPPFLAGS += -D x64_BIT

CFLAGS += -Werror -Wall
CFLAGS += -fno-stack-protector

FINDFLAGS ?= -name '*.c'
FINDFLAGS += -or -name '*.64.S'

LDFLAGS += -Wno-free-nonheap-object

default: gen/libc.a

on_error ?= do_nothing

ifeq ($(on_error), do_nothing)
ON_ERROR =
else ifeq ($(on_error), open_editor)
ON_ERROR += || ($$EDITOR $<; false)
else
$(error "invalid on_error option!");
endif

.PRECIOUS: %/

%/:
	mkdir -p $@

gen/srclist.mk: | gen/
	# echo "searching for source files..."
	find $(FINDFLAGS) | sort -Vr | sed 's/^/srcs += /' > $@

ifneq "$(MAKECMDGOALS)" "clean"
include gen/srclist.mk
endif

objs := $(patsubst %.c,gen/%.o,$(srcs))
objs := $(patsubst %.S,gen/%.o,$(objs))

deps := $(patsubst %.c,gen/%.d,$(srcs))
deps := $(patsubst %.S,gen/%.d,$(deps))

gen/libc.a: $(objs)
	ar rP -o $@ $^

gen/%.o gen/%.d: %.S | gen/%/
	$(CC) -c $(CPPFLAGS) $(ASFLAGS) $< -MD -o gen/$*.o $(ON_ERROR)

gen/%.o gen/%.d: %.c | gen/%/
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -MD -o gen/$*.o $(ON_ERROR)

install: gen/libc.a
	@ mkdir -p $(prefix)/lib $(prefix)/include
	cp -vau $< $(prefix)/lib/
	find -name '*.h' -print0 | xargs -0 cp -vau -t $(prefix)/include --parents

ifneq "$(MAKECMDGOALS)" "clean"
include $(deps)
endif






















