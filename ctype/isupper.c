
#include "isupper.h"

int isupper(int c)
{
	return 'A' <= c && c <= 'Z';
}

