
#include "islower.h"
#include "toupper.h"

int toupper(int c)
{
	return islower(c) ? ~0x20 & c : c;
}



