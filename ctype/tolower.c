
#include "isupper.h"
#include "tolower.h"

int tolower(int c)
{
	return isupper(c) ? 0x20 | c : c;
}



