
#include "isupper.h"
#include "islower.h"
#include "isalpha.h"

int isalpha(int c)
{
	return isupper(c) || islower(c);
}



