
#include <stdbool.h>

int isspace(int c)
{
	return false
		|| ' ' == c
		|| '\t' == c
		|| '\n' == c;
}

