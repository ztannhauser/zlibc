
#include <assert/assert.h>

#include <stddef/NULL.h>

#include <errno/program_invocation_name.h>

#include <stdio/stderr.h>
#include <stdio/fprintf.h>

#include <unistd/optind.h>
#include <unistd/optarg.h>

#include <string/strcmp.h>

#include <strings/index.h>

#include "option.h"
#include "getopt_long.h"

static char* nextchar = NULL;

static int optmov = 1;

static void slide(char** argv)
{
	int i = optind;
	char* arg = argv[optind];
	
	for (; i > 1 && argv[i - 1][0] != '-'; i--)
		argv[i] = argv[i - 1];
	
	argv[i] = arg;
	
	optind++, optmov++;
}

int getopt_long(
	int argc, char * const _argv[],
	const char *optstring,
	const struct option *longopts, int *longindex)
{
	char** argv = (typeof(argv)) _argv;
	
	while (optind < argc && argv[optind][0] != '-')
		optind++;
	
	if (optind == argc)
	{
		// move optind to the first non-option argument
		optind = optmov;
	}
	else if (argv[optind][1] != '-')
	{
		char* where;
		
		// short option(s):
		if (!nextchar)
			nextchar = argv[optind] + 1;
		
		if (index(":", *nextchar) || !(where = index(optstring, *nextchar)))
		{
			fprintf(stderr, "%s: invalid option -- '%c'\n",
				program_invocation_name, *nextchar);
			return '?';
		}
		else if (where[1] != ':')
		{
			// no_argument
			if (!*++nextchar)
			{
				nextchar = NULL;
				slide(argv);
			}
			
			return *where;
		}
		else if (where[2] != ':')
		{
			// required_argument
			if (nextchar[1])
			{
				assert(!"TODO");
			}
			else
			{
				slide(argv);
				optarg = argv[optind];
				slide(argv);
				return *where;
			}
		}
		else
		{
			// optional_argument
			assert(!"TODO");
		}
	}
	else if (argv[optind][2] != '\0')
	{
		char* longoption = argv[optind] + 2;
		
		char* equals = index(longoption, '=');
		
		if (equals)
			*equals++ = '\0';
		
		const struct option* found = NULL;
		
		for (*longindex = 0; !found && longopts[*longindex].name; (*longindex)++)
			if (!strcmp(longopts[*longindex].name, longoption)) {
				found = &longopts[*longindex];
				break;
			}
		
		if (!found)
			return '?';
		
		slide(argv);
		
		switch (found->has_arg)
		{
			case no_argument:
				if (equals)
				{
					fprintf(stderr, "%s: option '--%s' doesn't allow an "
						"argument\n", program_invocation_name, longoption);
					return '?';
				}
				break;
				
			case required_argument:
				assert(!"TODO");
				break;
				
			case optional_argument:
				assert(!"TODO");
				break;
		}
		
		if (found->flag)
			*found->flag = found->val;
		else
			return found->val;
	}
	
	return -1;
}






























