
#ifndef STRUCT_OPTION_H
#define STRUCT_OPTION_H

struct option
{
	const char *name;
	enum {
		no_argument,
		required_argument,
		optional_argument,
	} has_arg;
	int *flag;
	int val;
};

#endif
