struct winsize
{
	unsigned short ws_row;
	unsigned short ws_col;
	unsigned short ws_xpixel;   /* unused */
	unsigned short ws_ypixel;   /* unused */
};

