
#ifndef FCNTL_H
#define FCNTL_H
// The <fcntl.h> header shall define the following symbolic constants for the
// cmd argument used by fcntl().  The values shall be unique and shall be
// suitable for use in #if preprocessing directives.

// F_DUPFD
	// Duplicate file descriptor.
#include <fcntl/F_DUPFD.h>

// F_DUPFD_CLOEXEC
	// Duplicate file descriptor with the close-on-exec flag FD_CLOEXEC set.
#include <fcntl/F_DUPFD_CLOEXEC.h>

// F_GETFD
	// Get file descriptor flags.
#include <fcntl/F_GETFD.h>

// F_SETFD
	// Set file descriptor flags.
#include <fcntl/F_SETFD.h>

// F_GETFL
	// Get file status flags and file access modes.
#include <fcntl/F_GETFL.h>

// F_SETFL
	// Set file status flags.
#include <fcntl/F_SETFL.h>

// F_GETLK
	// Get record locking information.
#include <fcntl/F_GETLK.h>

// F_SETLK
	// Set record locking information.
#include <fcntl/F_SETLK.h>

// F_SETLKW
	// Set record locking information; wait if blocked.
#include <fcntl/F_SETLKW.h>

// F_GETOWN
	// Get process or process group ID to receive SIGURG signals.
#include <fcntl/F_GETOWN.h>

// F_SETOWN
	// Set process or process group ID to receive SIGURG signals.
#include <fcntl/F_SETOWN.h>

// The  <fcntl.h>  header  shall define the following symbolic constant used
// for the fcntl() file descriptor flags, which shall be suitable for use in
// #if preprocessing directives.

// FD_CLOEXEC
	// Close the file descriptor upon execution of an exec family function.
#include <fcntl/FD_CLOEXEC.h>

// The <fcntl.h> header shall also define the following symbolic constants for
// the l_type argument used for record  locking  with  fcntl(). The values shall
// be unique and shall be suitable for use in #if preprocessing directives.

// F_RDLCK
	// Shared or read lock.
#include <fcntl/F_RDLCK.h>

// F_UNLCK
	// Unlock.
#include <fcntl/F_UNLCK.h>

// F_WRLCK
	// Exclusive or write lock.
#include <fcntl/F_WRLCK.h>

// The <fcntl.h> header shall define the values used for l_whence, SEEK_SET,
// SEEK_CUR, and SEEK_END as described in <stdio.h>.
#include <stdio/SEEK_SET.h>
#include <stdio/SEEK_CUR.h>
#include <stdio/SEEK_END.h>

// The <fcntl.h> header shall define the following symbolic constants as file
// creation flags for use in the oflag value to open() and openat().  The
// values shall be bitwise-distinct and shall be suitable for use in #if
// preprocessing directives.

// O_CLOEXEC
	// The FD_CLOEXEC flag associated with the new descriptor shall be set to
	// close the file descriptor upon execution of  an  exec family function.
#include <fcntl/O_CLOEXEC.h>

// O_CREAT
	// Create file if it does not exist.
#include <fcntl/O_CREAT.h>

// O_DIRECTORY
	// Fail if not a directory.
#include <fcntl/O_DIRECTORY.h>

// O_EXCL
	// Exclusive use flag.
#include <fcntl/O_EXCL.h>

// O_NOCTTY
	// Do not assign controlling terminal.
#include <fcntl/O_NOCTTY.h>

// O_NOFOLLOW
	// Do not follow symbolic links.
#include <fcntl/O_NOFOLLOW.h>

// O_TRUNC
	// Truncate flag.
#include <fcntl/O_TRUNC.h>

// O_TTY_INIT
	// Set  the  termios  structure  terminal parameters to a state that
	// provides conforming behavior; see Section 11.2, Parameters that can be
	// set.
#include <fcntl/O_TTY_INIT.h>

// The O_TTY_INIT flag can have the value zero and in this case it need not be
// bitwise-distinct from the other flags.

// The <fcntl.h> header shall define the following symbolic constants for use
// as file status flags for open(), openat(), and fcntl(). The values shall be
// suitable for use in #if preprocessing directives.

// O_APPEND
	// Set append mode.
#include <fcntl/O_APPEND.h>

// O_DSYNC
	// Write according to synchronized I/O data integrity completion.
#include <fcntl/O_DSYNC.h>

// O_NONBLOCK
	// Non-blocking mode.
#include <fcntl/O_NONBLOCK.h>

// O_RSYNC
	// Synchronized read I/O operations.
#include <fcntl/O_RSYNC.h>

// O_SYNC
	// Write according to synchronized I/O file integrity completion.
#include <fcntl/O_SYNC.h>

// O_PATH
	// Zander: can't find documentation which puts this here, but Glibc does:
#include <fcntl/O_PATH.h>

// The <fcntl.h> header shall define the following symbolic constant for use
// as the mask for file access modes. The value shall be suitable for use in
// #if preprocessing directives.

// O_ACCMODE
	// Mask for file access modes.
#include <fcntl/O_ACCMODE.h>

// The <fcntl.h> header shall define the following symbolic constants for use
// as the file access modes for open(), openat(),  and  fcntl(). The  values
// shall be unique, except that O_EXEC and O_SEARCH may have equal values. The
// values shall be suitable for use in #if preprocessing directives.

// O_EXEC
	// Open for execute only (non-directory files). The result is unspecified
	// if this flag is applied to a directory.
#include <fcntl/O_EXEC.h>

// O_RDONLY
	// Open for reading only.
#include <fcntl/O_RDONLY.h>

// O_RDWR
	// Open for reading and writing.
#include <fcntl/O_RDWR.h>

// O_SEARCH
	// Open directory for search only. The result is unspecified if this flag
	// is applied to a non-directory file.
#include <fcntl/O_SEARCH.h>

// O_WRONLY
	// Open for writing only.
#include <fcntl/O_WRONLY.h>

// The <fcntl.h> header shall define the symbolic constants for file modes for
// use as values of mode_t as described in <sys/stat.h>.
#include <sys/stat/S_IRWXU.h>
#include <sys/stat/S_IRUSR.h>
#include <sys/stat/S_IWUSR.h>
#include <sys/stat/S_IXUSR.h>
#include <sys/stat/S_IRWXG.h>
#include <sys/stat/S_IRGRP.h>
#include <sys/stat/S_IWGRP.h>
#include <sys/stat/S_IXGRP.h>
#include <sys/stat/S_IRWXO.h>
#include <sys/stat/S_IROTH.h>
#include <sys/stat/S_IWOTH.h>
#include <sys/stat/S_IXOTH.h>
#include <sys/stat/S_ISUID.h>
#include <sys/stat/S_ISGID.h>
#include <sys/stat/S_ISVTX.h>

// The <fcntl.h> header shall define the following symbolic constant as a
// special value used in place of a file descriptor  for  the  *at()
// functions which take a directory file descriptor as a parameter:

// AT_FDCWD
	// Use the current working directory to determine the target of relative
	// file paths.
#include <fcntl/AT_FDCWD.h>

// The <fcntl.h> header shall define the following symbolic constant as a value
// for the flag used by faccessat():

// AT_EACCESS
	// Check access using effective user and group ID.
#include <fcntl/AT_EACCESS.h>

// The <fcntl.h> header shall define the following symbolic constant as a value
// for the flag used by fstatat(), fchmodat(), fchownat(), and utimensat():

// AT_SYMLINK_NOFOLLOW
	// Do not follow symbolic links.
#include <fcntl/AT_SYMLINK_NOFOLLOW.h>

// The <fcntl.h> header shall define the following symbolic constant as a value
// for the flag used by linkat():

// AT_SYMLINK_FOLLOW
	// Follow symbolic link.
#include <fcntl/AT_SYMLINK_FOLLOW.h>

// The <fcntl.h> header shall define the following symbolic constant as a value
// for the flag used by unlinkat():

// AT_REMOVEDIR
	// Remove directory instead of file.
#include <fcntl/AT_REMOVEDIR.h>

// The <fcntl.h> header shall define the following symbolic constants for the
// advice argument used by posix_fadvise():

// POSIX_FADV_DONTNEED
	// The application expects that it will not access the specified data in the
	// near future.
#include <fcntl/POSIX_FADV_DONTNEED.h>

// POSIX_FADV_NOREUSE
	// The application expects to access the specified data once and then not
	// reuse it thereafter.
#include <fcntl/POSIX_FADV_NOREUSE.h>

// POSIX_FADV_NORMAL
	// The application has no advice to give on its behavior with respect to the
	// specified data. It is the default characteristic  if  no advice is given
	// for an open file.
#include <fcntl/POSIX_FADV_NORMAL.h>

// POSIX_FADV_RANDOM
	// The application expects to access the specified data in a random order.
#include <fcntl/POSIX_FADV_RANDOM.h>

// POSIX_FADV_SEQUENTIAL
	// The application expects to access the specified data sequentially from
	// lower offsets to higher offsets.
#include <fcntl/POSIX_FADV_SEQUENTIAL.h>

// POSIX_FADV_WILLNEED
	// The application expects to access the specified data in the near future.
#include <fcntl/POSIX_FADV_WILLNEED.h>

// The <fcntl.h> header shall define the flock structure describing a file lock.
// It shall include the following members:

// short  l_type   Type of lock; F_RDLCK, F_WRLCK, F_UNLCK.
// short  l_whence Flag for starting offset.
// off_t  l_start  Relative offset in bytes.
// off_t  l_len    Size; if 0 then until EOF.
// pid_t  l_pid    Process ID of the process holding the lock; returned with F_GETLK.

#include <fcntl/flock.h>

// The <fcntl.h> header shall define the mode_t, off_t, and pid_t types as
// described in <sys/types.h>.
#include <sys/types/mode_t.h>
#include <sys/types/off_t.h>
#include <sys/types/pid_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.
#include <fcntl/creat.h>
#include <fcntl/fcntl.h>
#include <fcntl/open.h>
#include <fcntl/openat.h>
#include <fcntl/posix_fadvise.h>
#include <fcntl/posix_fallocate.h>

// Inclusion of the <fcntl.h> header may also make visible all symbols from
// <sys/stat.h> and <unistd.h>.

















#endif
