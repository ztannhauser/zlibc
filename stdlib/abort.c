
#include <signal/raise.h>
#include <signal/SIGABRT.h>

#include "abort.h"

void abort(void)
{
	raise(SIGABRT);
	while(1);
}

