
#include <stddef/size_t.h>

void         *bsearch(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));
