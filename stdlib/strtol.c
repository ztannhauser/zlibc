
#include <ctype/isspace.h>

#include <errno/errno.h>
#include <errno/ERANGE.h>

#include <limits/LONG_MAX.h>
#include <limits/LONG_MIN.h>

#include "strtoul.h"
#include "strtol.h"

long strtol(const char* s, char** endptr, int base)
{
	const char* sc;
	unsigned long x;
	
	for (sc = s; isspace(*sc); ++sc);
	
	x = strtoul(s, endptr, base);
	
	if (*sc == '-' && x <= LONG_MAX)
		return errno = ERANGE, LONG_MIN;
	else if (*sc != '-' && LONG_MAX < x)
		return errno = ERANGE, LONG_MAX;
	else
		return (long) x;
}

