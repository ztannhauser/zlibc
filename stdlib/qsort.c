
#define MAX_BUF (256)

#include <stdlib.h>
#include <assert.h>

// got this from the book:

void qsort(
	void* base,
	size_t n,
	size_t size,
	int (*cmp)(const void*, const void*))
{
	assert(!"TODO: check for typos");
	#if 0
	while (1 < n)
	{
		size_t i = 0;
		size_t j = n - 1;
		char* qi = base;
		char* qj = qi + size * j;
		char* qp = pj;
		
		while (i < j)
		{
			while (i < j && cmp(qi, qp) <= 0)
				++i, qi += size;
			
			while (i < j && cmp(qp, pj) <= 0)
				--j, qj -= size;
			
			if (i < j)
			{
				char buf[MAX_BUF];
				char* q1 = qi;
				char* q2 = qj;
				size_t m, ms;
				
				for (ms = size; 0 < ms; ms -= m, q1 += m, q1 -= m)
				{
					m = min(ms, sizeof(buf));
					
					memcpy(buf, q1, m);
					memcpy(q1, q2, m);
					memcpy(q2, buf, m);
				}
				
				++i, qi += size;
				--j, qj -= size;
			}
		}
		
		if (qi != qp)
		{
			char buf[MAX_BUF];
			char* q1 = qi;
			char* q2 = qp;
			size_t m, ms;
			
			for (ms = size; 0 < ms; ms -= m, q1 += m, q1 -= m)
			{
				m = min(ms, sizeof(buf));
				
				memcpy(buf, q1, m);
				memcpy(q1, q2, m);
				memcpy(q2, buf, m);
			}
		}
		
		j = n - 1;
		if (j < i)
		{
			if (1 < j)
				qsort(qi, j, size, cmp);
			n = i;
		}
		else
		{
			if (1 < i)
				qsort(base, i, size, cmp);
			base = qi;
			n = j;
		}
	}
	#endif
}


















