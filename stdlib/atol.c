
#include <stddef/NULL.h>

#include "strtoul.h"
#include "atol.h"

long atol(const char* s)
{
	return (long) strtoul(s, NULL, 10);
}

