
#include <stdlib.h>

// got this from the book:

void* bsearch(
	const void* key,
	const void* base,
	size_t nelem,
	size_t size,
	int (*cmp)(const void*, const void*))
{
	const char* p = base;
	size_t n;
	
	for (p = base, n = nelem; 0 < n; )
	{
		const size_t pivot = n >> 1;
		const char* const q = p + size * pivot;
		const int val = cmp(key, q);
		
		if (val < 0)
			n = pivot;
		else if (!val)
			return (void*) q;
		else {
			p = q + size,
			n -= pivot + 1;
		}
	}
	
	return NULL;
}














