
#include <stddef/NULL.h>

#include <unistd/globals.h>

#include <string/strncmp.h>
#include <string/strlen.h>

#include "getenv.h"

char* getenv(const char *name)
{
	size_t len = strlen(name);
	
	for (char** e = environ; *e; e++)
		if (!strncmp(*e, name, len) && len[*e] == '=')
			return *e + len + 1;
	
	return NULL;
}











