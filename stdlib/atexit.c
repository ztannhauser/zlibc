
#include <assert/assert.h>

#include <stdlib/malloc.h>

#include "registered_exits.h"
#include "atexit.h"

int atexit(void (*function)(void))
{
	struct registered_exits_link* link;
	
	if (!(link = malloc(sizeof(*link))))
		return -1;
	
	link->function = function;
	link->prev = registered_exits_head;
	registered_exits_head = link;
	
	return 0;
}



