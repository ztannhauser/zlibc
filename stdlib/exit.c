

#include <stdio/fflush.h>
#include <stdio/stdin.h>
#include <stdio/stdout.h>
#include <stdio/stderr.h>

#include <stdlib/free.h>

#include <assert/assert.h>

#include <unistd/_exit.h>

#include "registered_exits.h"

void exit(int status)
{
	// call registered exit functions:
	{
		struct registered_exits_link* prev;
		
		while (registered_exits_head)
		{
			registered_exits_head->function();
			
			prev = registered_exits_head->prev;
			
			free(registered_exits_head);
			
			registered_exits_head = prev;
		}
	}
	
	fflush(stdin);
	fflush(stdout);
	fflush(stderr);
	
	_exit(status);
}














