
#include <ctype/isspace.h>
#include <ctype/tolower.h>

#include <errno/errno.h>
#include <errno/ERANGE.h>

#include <stddef/ptrdiff_t.h>

#include <string/memchr.h>

#include <limits/ULONG_MAX.h>

#include "strtoul.h"

#define BASE_MAX (36)

static const char digits[] = 
	"0123456789" "abcdefghijklmnopqustuvwxyz";

static const char ndigs[BASE_MAX + 1] = {
	0, 0, 65, 42, 33, 29, 26, 24, 23, 22, 21, 20, 19, 19, 18, 18, 17, 17, 17,
	17, 16, 16, 16, 16, 15, 15, 15, 15, 15, 15, 15, 14, 14, 14, 14, 14, 14};

// got this from the book (page 360):

unsigned long int strtoul(const char *s, char **endptr, int base)
{
	const char *sc, *sd;
	const char *s1, *s2;
	char sign;
	ptrdiff_t n;
	unsigned long x, y = 0;
	
	for (sc = s; isspace(*sc); ++sc);
	
	sign = *sc == '-' || *sc == '+' ? *sc++ : '+';
	
	if (base < 0 || base == 1 || BASE_MAX < base)
	{
		if (endptr)
			*endptr = (char*) s;
		return 0;
	}
	else if (base)
	{
		if (base == 16 && *sc == '0' && (sc[1] == 'x' || sc[1] == 'X'))
			sc += 2;
	}
	else if (*sc != '0')
		base = 10;
	else if (sc[1] == 'x' || sc[1] == 'X')
		base = 16;
	else
		base = 8;
	
	for (s1 = sc; *sc == '0'; ++sc);
	
	x = 0;
	
	for (s2 = sc; (sd = memchr(digits, tolower(*sc), base)); ++sc)
	{
		y = x;
		x = x * base + (sd - digits);
	}
	
	if (s1 == sc)
	{
		if (endptr)
			*endptr = (char*) s;
		return 0;
	}
	
	n = sc - s2 - ndigs[base];
	
	if (n < 0)
		;
	else if (0 < n || x < x - (sd - digits) || (x - (sd - digits)) / base != y)
	{
		errno = ERANGE;
		x = ULONG_MAX;
	}
	
	if (sign == '-')
		x = -x;
	
	if (endptr)
		*endptr = (char*) sc;
	
	return x;
}






























