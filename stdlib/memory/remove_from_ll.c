
#include <assert/assert.h>

#include "header.h"
#include "globals.h"
#include "remove_from_ll.h"

void remove_from_ll(struct memory_header* block)
{
	assert(!block->is_alloc);
	
	if (block->prev)
		block->prev->next = block->next;
	else
		free_list_head = block->next;
	
	if (block->next)
		block->next->prev = block->prev;
	else
		free_list_tail = block->prev;
}

