
#include <stddef/NULL.h>

#include <stdbool/false.h>

#include <assert/assert.h>

#include <stdio/stdout.h>
#include <stdio/fprintf.h>
#include <stdio/fflush.h>
#include <stdio/stderr.h>

#include <stdlib/abort.h>

#include <errno/program_invocation_name.h>

#include <unistd/memory_globals.h>

#ifdef DEBUG_BUILD
#include <debug.h>
#endif

#include "BUFFER_OVERFLOW_CHECK.h"
#include "globals.h"
#include "header.h"
#include "footer.h"
#include "remove_from_ll.h"
#include "free.h"

void free(void *ptr)
{
	if (ptr)
	{
		struct memory_header* header;
		struct memory_footer* footer;
		
		header = ptr - sizeof(*header);
		footer = ((void*) header) + header->size - sizeof(*footer);
		
		{
			struct memory_header* preheader;
			struct memory_footer* prefooter;
			
			if ((void*) header > program_break_start &&
				!(preheader =
				 (prefooter = (void*) header - sizeof(*prefooter))
				  ->header)->is_alloc)
			{
				// merge upwards
				remove_from_ll(preheader);
				
				header = preheader;
			}
		}
		
		{
			struct memory_header* postheader;
			
			if ((void*) (footer + 1) < program_break_end &&
				!(postheader = (void*) footer + sizeof(*footer))->is_alloc)
			{
				// remove from linked list
				remove_from_ll(postheader);
				
				footer = (void*) postheader + postheader->size - sizeof(*footer);
			}
		}
		
		header->is_alloc = false;
		header->size = (void*) footer - (void*) header + sizeof(*footer);
		
		header->next = free_list_head;
		header->prev = NULL;
		
		footer->header = header;
		
		if (free_list_head)
			free_list_head->prev = header;
		else
			free_list_tail = header;
		
		free_list_head = header;
	}
	
	#ifdef DEBUG_BUILD
	ddprintf("\e[37m" "free(%p);" "\e[m" "\n", ptr);
	#endif
}










