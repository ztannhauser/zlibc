
#include <stddef/size_t.h>
#include <stdbool/bool.h>

struct memory_header
{
	bool is_alloc;
	
	size_t size;
	
	struct memory_header *prev, *next;
};

