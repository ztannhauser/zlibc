
#if 0
#ifdef DEBUG_BUILD

#include <stdio.h>

#include "globals.h"
#include "header.h"
#include "footer.h"
#include "memleak_check.h"

void memleak_check()
{
	size_t total = 0;
	
	for (struct memory_header *i = malloc_list_head; i; i = i->next)
	{
		size_t size = 0
			+ i->size
			- sizeof(struct memory_header)
			- sizeof(struct memory_footer);
		
		total += size;
		
		printf("%lu@%p: %s:%u\n", size, i + 1, i->file, i->line);
	}
	
	if (total)
	{
		printf("total bytes leaked: %lu\n", total);
	}
}

#endif
#endif
