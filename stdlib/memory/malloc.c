
#include <stdbool/false.h>
#include <stdbool/true.h>

#include <assert/assert.h>

#include <stdio/stdout.h>
#include <stdio/fflush.h>

#include <stddef/NULL.h>

#include <unistd/sbrk.h>

#ifdef DEBUG_BUILD
#include <debug.h>
#endif

#include "memleak_check.h"
#include "BUFFER_OVERFLOW_CHECK.h"
#include "remove_from_ll.h"
#include "globals.h"
#include "header.h"
#include "footer.h"
#include "malloc.h"

static void private_arena_find_block(
	void** out, size_t* size)
{
	void* ptr;
	struct memory_header *i, *block = NULL;
	
	for (i = free_list_head; !block && i; i = i->next)
		if (!i->is_alloc && *size <= i->size)
			ptr = block = i;
	
	if (block)
	{
		
		if (block->size >= 0
			+ sizeof(struct memory_header)
			+ *size
			+ sizeof(struct memory_footer))
		{
			size_t split_size = block->size - *size;
			
			struct memory_header* newblock = ptr + *size;
			struct memory_footer* footer = ptr + block->size - sizeof(*footer);
			
			// do split, adjust linked-list:
			newblock->is_alloc = false;
			newblock->size = split_size;
			
			newblock->prev = block->prev;
			newblock->next = block->next;
			
			footer->header = newblock;
			
			if (block->prev)
				block->prev->next = newblock;
			else
				free_list_head = newblock;
			
			if (block->next)
				block->next->prev = newblock;
			else
				free_list_tail = newblock;
		}
		else
		{
			// don't do split, remove from linked-list
			
			remove_from_ll(block);
			
			*size = block->size;
		}
		
		*out = ptr;
	}
	
	// if there's no available free blocks call sbrk
	// to get another one.
	else if ((ptr = block = sbrk(*size)) == (void*) -1)
	{
		assert(!"TODO");
	}
	else
	{
		*out = ptr;
	}
}

void* malloc(size_t user_size)
{
	void* ptr = NULL;
	size_t block_size;
	struct memory_header* header;
	struct memory_footer* footer;
	void* payload = NULL;
	
	if (user_size)
	{
		block_size = 0 + sizeof(*header) + user_size + sizeof(*footer);
		
		private_arena_find_block(&ptr, &block_size);
	}
	
	if (ptr)
	{
		header = ptr;
		footer = ptr + block_size - sizeof(*footer);
		
		header->is_alloc = true;
		header->size = block_size;
		
		footer->header = header;
		
		payload = ptr + sizeof(*header);
		
		#ifdef DEBUG_BUILD
	/*	VALGRIND_MAKE_MEM_UNDEFINED(payload, block_size - sizeof(*footer) - sizeof(*header));*/
		#endif
	}
	
	#ifdef DEBUG_BUILD
	ddprintf("\e[37m" "malloc(size = %lu) => %p" "\e[m" "\n", user_size, payload);
	#endif
	
	return payload;
}






















