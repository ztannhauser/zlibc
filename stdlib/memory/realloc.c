
#include <assert/assert.h>

#include <stdbool/true.h>
#include <stdbool/false.h>

#include <stdio/stdout.h>
#include <stdio/fprintf.h>
#include <stdio/stderr.h>
#include <stdio/fflush.h>

#include <stddef/NULL.h>

#include <string/memcpy.h>

#include <stdlib/abort.h>

#include <errno/program_invocation_name.h>

#include "../../unistd/memory_globals.h"

#include "BUFFER_OVERFLOW_CHECK.h"

#include "header.h"
#include "footer.h"
#include "malloc.h"
#include "free.h"
#include "realloc.h"

void *realloc(void *ptr, size_t size)
{
	struct memory_header* header;
	struct memory_footer* footer;
	size_t requested_size;
	
	assert(!"CHECK");
	
	if (!ptr)
		return malloc(size);
	else if (!size)
		return free(ptr), NULL;
	else if ((header = ptr - sizeof(*header))->size < (requested_size = 0
		+ size
		+ sizeof(*header)
		+ sizeof(*footer)))
	{
		footer = (void*) header + header->size - sizeof(*footer);
		
		struct memory_header* postheader;
		
		if (true
			&& (void*) (footer + 1) < program_break_end
			&& !(postheader = (void*) footer + sizeof(*footer))->is_alloc
			&& header->size + postheader->size <= requested_size)
		{
			// merge downwards
			// remove from linked list
			
			#if 0
			remove_from_ll(postheader);
			
			footer = (void*) postheader + postheader->size - sizeof(*footer);
			#endif
			
			fflush(stdout);
			assert(!"TODO");
		}
		else
		{
			void* new = malloc(size);
			
			if (!new)
				return NULL;
			
			memcpy(new, ptr, size);
			free(ptr);
			
			return new;
		}
	}
	else
		return ptr;
}














