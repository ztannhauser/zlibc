
#include <stdlib/RAND_MAX.h>

#include "randseed.h"
#include "rand.h"

int rand()
{
	randseed = randseed * 1103515245 + 12345;
	return ((unsigned int)(randseed >> 16)) & RAND_MAX;
}

