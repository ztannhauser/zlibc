
#ifndef STRINGS_H
#define STRINGS_H

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided for use with ISO C standard
// compilers.
#include <strings/ffs.h>
#include <strings/index.h>
#include <strings/rindex.h>
#include <strings/strcasecmp.h>
#include <strings/strcasecmp_l.h>
#include <strings/strncasecmp.h>
#include <strings/strncasecmp_l.h>

// The <strings.h> header shall define the locale_t type as described in
// <locale.h>.
#include <locale/locale_t.h>

// The <strings.h> header shall define the size_t type as described in
// <sys/types.h>.
#include <sys/types/size_t.h>

#endif
