
#include <stdio.h>
#include <stdarg.h>

int fprintf(FILE *stream, const char *format, ...)
{
	int retval;
	
	va_list ap;
	
	va_start(ap, format);
	
	retval = vfprintf(stream, format, ap);
	
	va_end(ap);
	
	return retval;
}


