
#include <stdbool/true.h>
#include <stdbool/false.h>

#include <fcntl/O_RDONLY.h>

#include "stdin.h"

FILE* stdin = &(struct FILE) {
	.fd = 0,
	.oflags = O_RDONLY,
	.pos = 0,
	.i = 0,
	.n = 0,
	.is_seekable = false,
	.is_unbuffered = false,
	.is_allocd = false,
	.error_flag = false,
	.eof_flag = false,
};

