
#include <stdarg/va_start.h>
#include <stdarg/va_end.h>

#include "vsprintf.h"
#include "sprintf.h"

int sprintf(char *str, const char *fmt, ...)
{
	int retval;
	va_list vargs;
	
	va_start(vargs, fmt);
	
	retval = vsprintf(str, fmt, vargs);
	
	va_end(vargs);
	
	return retval;
}

