
#include <assert.h>

#include "fileno.h"

int fileno(FILE *stream)
{
	return stream->fd;
}

