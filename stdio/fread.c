
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/param.h>
#include <stdbool.h>
#include <unistd.h>
#include <limits.h>

#include "FILE.h"

#define READ_MAX (0x7ffff000)

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t a, b, c = 0, len = size * nmemb;
	
	if ((stream->oflags & O_ACCMODE) == O_WRONLY)
	{
		// not opened for reading
		return EOF;
	}
	else if (stream->is_unbuffered)
	{
		while (len)
		{
			a = MIN(len, READ_MAX);
			
			if ((b = read(stream->fd, ptr, len)) < 0)
			{
				stream->error_flag = true;
				break;
			}
			else if (!b)
			{
				stream->eof_flag = true;
				break;
			}
			else
			{
				stream->pos += b;
				c += b, ptr += b, len -= b;
			}
		}
	}
	else while (len)
	{
		a = MIN(len, stream->n - stream->i);
		memcpy(ptr, stream->buffer + stream->i, a);
		stream->i += a, c += a, ptr += a, len -= a;
		
		if (!len);
		else if (true
			&& stream->is_seekable // otherwise: it's a (read-only) stream
			&& stream->is_dirty // otherwise: there's nothing new to write about
			&& pwrite(stream->fd, stream->buffer, stream->n, stream->pos) < 0)
		{
			stream->error_flag = true;
			break;
		}
		else if ((b = read(stream->fd, stream->buffer, BUFSIZ)) < 0)
		{
			stream->error_flag = true;
			break;
		}
		else if (!b)
		{
			stream->eof_flag = true;
			break;
		}
		else
		{
			stream->is_dirty = false;
			stream->pos += stream->n;
			stream->n = b;
			stream->i = 0;
		}
	}
	
	return c;
}
















