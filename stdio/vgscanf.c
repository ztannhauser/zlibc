
#include <stdbool/bool.h>
#include <stdbool/false.h>
#include <stdbool/true.h>

#include <stddef/NULL.h>
#include <stddef/size_t.h>
#include <stddef/ptrdiff_t.h>

#include <strings/index.h>

#include <ctype/isspace.h>
#include <ctype/isdigit.h>

#include <assert/assert.h>

#include <stdint/uintmax_t.h>

#include <stdarg/va_arg.h>

#include "stdout.h"
#include "fflush.h"
#include "EOF.h"

#include "vgscanf.h"

int vgscanf(
	void* ptr,
	int (*getc)(void* ptr),
	int (*ungetc)(int c, void* ptr),
	const char* fmt, va_list vargs)
{
	char f = *fmt++;
	char c;
	int count = 0;
	
	if ((c = getc(ptr)) == EOF)
		return EOF;
	else while (f)
	{
		if (isspace(f))
		{
			// A sequence of white-space characters (space, tab, newline, etc.;
			// see isspace(3)).  This directive matches  any  amount  of  white
			// space, including none, in the input.
			
			while (isspace(c))
				c = getc(ptr);
		}
		else if (f == '%')
		{
			f = *fmt++;
			
			// A conversion specification, which commences with a '%' (percent)
			// character.  A sequence of characters from the input is converted
			// according to this specification, and the result is placed in the
			// corresponding pointer argument.  If the next item of input  does
			// not match the conversion specification, the conversion fails-this
			// is a matching failure.
			
			// Each conversion specification in format begins with either the
			// character '%' or the character sequence "%n$" (see below for the
			// distinction) followed by:
			
			// An optional '*' assignment-suppression character: scanf() reads
			// input as directed by the conversion specification,  but  discards
			// the  input.  No corresponding pointer argument is required, and
			// this specification is not included in the count of successful
			// assignments returned by scanf().
			
			// For decimal conversions, an optional quote character (').
			// This specifies that the input number may include thousands'
			// separators as defined by the LC_NUMERIC category of the current
			// locale. (See setlocale(3).) The quote character may precede or
			// follow the '*' assignment-suppression character:
			
			bool assignment_suppressed = false;
			bool thousands = false;
			
			while (!f && index("*'", f))
			{
				switch (f)
				{
					case '*': assignment_suppressed = true; break;
					case '\'': thousands = true; break;
				}
				
				f = *fmt++;
			}
			
			// An optional 'm' character.  This is used with string conversions
			// (%s, %c, %[), and relieves the caller of the need to allocate  a
			// corresponding  buffer  to hold the input: instead, scanf()
			// allocates a buffer of sufficient size, and assigns the address
			// of this buffer to the corresponding pointer argument, which
			// should be a pointer to a char * variable (this variable does not
			// need  to  be initialized before the call).  The caller should
			// subsequently free(3) this buffer when it is no longer required.
			
			bool allocate = false;
			if (f == 'm')
			{
				allocate = true;
				f = *fmt++;
			}
			
			// An  optional  decimal  integer  which specifies the maximum
			// field width.  Reading of characters stops either when this
			// maximum is reached or when a nonmatching character is found,
			// whichever happens first.  Most conversions discard initial
			// white space  characters  (the  exceptions are noted below), and
			// these discarded characters don't count toward the maximum field
			// width.  String input conversions store a terminating null byte
			// ('\0') to mark the end of the input; the maximum field width
			// does not include this terminator.
			
			struct {
				bool is_set;
				size_t value;
			} max_field_width = {false};
			
			if (isdigit(f))
			{
				max_field_width.is_set = true;
				max_field_width.value = 0;
				do max_field_width.value = max_field_width.value * 10 + f - '0';
				while (isdigit(f = *fmt++));
			}
			
			// An  optional  type  modifier  character.  For example, the l
			// type modifier is used with integer conversions such as %d to
			// specify that the corresponding pointer argument refers to a
			// long int rather than a pointer to an int.
			
			enum length_modifier {
				lm_char, lm_short, lm_int, lm_long, lm_long_long,
				lm_intmax, lm_size, lm_ptrdiff
			} lm = lm_int;
			
			// The following type modifier characters can appear in a
			// conversion specification:
			
			switch (f)
			{
				case 'h':
					// h: Indicates that the conversion will be one of d, i, o, u,
					// x, X, or n and the next pointer is a pointer to a short
					// int or unsigned short int (rather than int).
					// hh: As for h, but the next pointer is a pointer to a
					// signed char or unsigned char.
				{
					f = *fmt++, lm = lm_short;
					
					if (f == 'h')
					{
						lm = lm_char;
						f = *fmt++;
					}
					break;
				}
				
				case 'j':
					// j: As for h, but the next pointer is a pointer to an
					// intmax_t or a uintmax_t.
					// This modifier was introduced in C99.
				{
					f = *fmt++, lm = lm_intmax;
					break;
				}
				
				case 'l':
					// l: Indicates either that the conversion will be one of
					// d, i, o, u, x, X, or n and the next pointer is a pointer
					// to a long int or unsigned long int (rather than int), or
					// that the conversion will be one of e, f, or g and the
					// next pointer is a pointer to double (rather than float).
					// Specifying two l characters is equivalent to L. If used
					// with %c or %s, the corresponding parameter is
					// considered as a pointer to a wide character or
					// wide-character string respectively.
				{
					f = *fmt++, lm = lm_long;
					
					if (f == 'l')
					{
						lm = lm_long_long;
						f = *fmt++;
					}
					break;
				}
				
				case 'L':
					// L: Indicates that the conversion will be either e, f, or
					// g and the next pointer is a pointer to long double or
					// the conversion will be d, i, o, u, or x and the next
					// pointer is a pointer to long long.
				case 'q':
					// q: equivalent to L.
					// This specifier does not exist in ANSI C.
				{
					f = *fmt++, lm = lm_long_long;
					break;
				}
				
				case 't':
					// t: As for h, but the next pointer is a pointer to a
					// ptrdiff_t.
					// This modifier was introduced in C99.
				{
					f = *fmt++, lm = lm_ptrdiff;
					break;
				}
				
				case 'z':
					// z: As for h, but the next pointer is a pointer to a
					// size_t.
					// This modifier was introduced in C99.
				{
					f = *fmt++, lm = lm_size;
					break;
				}
				
				default:
					break;
			}
			
			// A conversion specifier that specifies the type of input
			// conversion to be performed.
			
			// The following conversion specifiers are available:
			switch (f)
			{
				case '%':
					// Matches a literal '%'. That is, %% in the format
					// string matches a single input '%' character. No
					// conversion is done (but initial white space characters
					// are discarded), and assignment does not occur.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'd':
					// Matches an optionally signed decimal integer; the next
					// pointer must be a pointer to int.
				{
					if (thousands)
					{
						assert(!"TODO");
					}
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'D':
					// Equivalent to ld; this exists only for backward
					// compatibility. (Note: thus only in libc4. In libc5 and
					// glibc the %D is silently ignored, causing old programs
					// to fail mysteriously.)
				{
					if (thousands)
					{
						assert(!"TODO");
					}
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'i':
					// Matches an optionally signed integer; the next pointer
					// must be a pointer to int. The integer is read in base
					// 16 if it begins with 0x or 0X, in base 8 if it begins
					// with 0, and in base 10 otherwise. Only characters that
					// correspond to the base are used.
				{
					if (thousands)
					{
						assert(!"TODO");
					}
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'o':
					// Matches an unsigned octal integer; the next pointer must
					// be a pointer to unsigned int.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'u':
					// Matches an unsigned decimal integer; the next pointer
					// must be a pointer to unsigned int.
				{
					if (thousands)
					{
						assert(!"TODO");
					}
					
					while (isspace(c)) c = getc(ptr);
					
					uintmax_t value = 0;
					
					if (!isdigit(c))
						return EOF;
					else do {
						value = value * 10 + c - '0';
						c = getc(ptr);
					} while (true
						&& (max_field_width.is_set ? max_field_width.value-- : true)
						&& isdigit(c));
					
					if (!assignment_suppressed)
					{
						switch (lm)
						{
							case lm_char:      *va_arg(vargs, unsigned      char*) = value; break;
							case lm_short:     *va_arg(vargs, unsigned     short*) = value; break;
							case lm_int:       *va_arg(vargs, unsigned       int*) = value; break;
							case lm_long:      *va_arg(vargs, unsigned      long*) = value; break;
							case lm_long_long: *va_arg(vargs, unsigned long long*) = value; break;
							case lm_intmax:    *va_arg(vargs,          uintmax_t*) = value; break;
							case lm_size:      *va_arg(vargs,             size_t*) = value; break;
							case lm_ptrdiff:   *va_arg(vargs,          ptrdiff_t*) = value; break;
						}
						
						count++;
					}
					break;
				}
				
				case 'x':
					// Matches an unsigned hexadecimal integer; the next
					// pointer must be a pointer to unsigned int.
				case 'X':
					// Equivalent to x.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'f':
					// Matches an optionally signed floating-point number; the
					// next pointer must be a pointer to float.
				case 'e':
					// Equivalent to f.
				case 'g':
					// Equivalent to f.
				case 'E':
					// Equivalent to f.
				case 'a':
					// (C99) Equivalent to f.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 's':
					// Matches a sequence of non-white-space characters; the
					// next pointer must be a pointer to the initial element of
					// a character array that is long enough to hold the input
					// sequence and the terminating null byte ('\0'), which is
					// added automatically. The input string stops at white
					// space or at the maximum field width, whichever occurs
					// first.
				{
					if (allocate)
					{
						fflush(stdout);
						assert(!"TODO");
					}
					else
					{
						fflush(stdout);
						assert(!"TODO");
					}
					break;
				}
				
				case 'c':
					// Matches a sequence of characters whose length is
					// specified by the maximum field width (default 1); the
					// next pointer must be a pointer to char, and there must
					// be enough room for all the characters (no terminating
					// null byte is added). The usual skip of leading white
					// space is suppressed. To skip white space first, use an
					// explicit space in the format.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case '[':
					// Matches a nonempty sequence of characters from the
					// specified set of accepted characters; the next pointer
					// must be a pointer to char, and there must be enough room
					// for all the characters in the string, plus a terminating
					// null byte. The usual skip of leading white space is
					// suppressed. The string is to be made up of characters
					// in (or not in) a particular set; the set is defined by
					// the characters between the open bracket [ character and
					// a close bracket ] character. The set excludes those
					// characters if the first character after the open bracket
					// is a circumflex (^). To include a close bracket in the
					// set, make it the first character after the open bracket
					// or the circumflex; any other position will end the set.
					// The hyphen character - is also special; when placed
					// between two other characters, it adds all intervening
					// characters to the set. To include a hyphen, make it the
					// last character before the final close bracket. For
					// instance, [^]0-9-] means the set "everything except
					// close bracket, zero through nine, and hyphen".  The
					// string ends with the appearance of a character not in
					// the (or, with a circumflex, in) set or when the field
					// width runs out.
				{
					f = *fmt++;
					
					bool invert = false;
					
					// make room for the null:
					if (max_field_width.is_set)
						max_field_width.value--;
					
					if (f == '^')
					{
						invert = true;
						f = *fmt++;
					}
					
					const char* start = fmt - 1;
					
					do f = *fmt++; while (f != ']');
					
					const char* end = fmt - 1;
					
					// dpvsn(start, end - start);
					
					bool search(char c)
					{
						for (const char* i = start; i < end; i++)
							if (*i == '-' && i + 1 < end
								? i[-1] <= c && c <= i[+1] : *i == c)
								return true;
						
						return false;
					}
					
					char* out = NULL;
					
					if (!assignment_suppressed)
						out = va_arg(vargs, char*), count++;
					
					while (true
						&& (max_field_width.is_set ? max_field_width.value-- : true)
						&& (search(c) != invert))
					{
						if (out)
							*out++ = c;
						
						c = getc(ptr);
					}
					
					if (out)
						*out++ = '\0';
					
					break;
				}
				
				case 'p':
					// Matches a pointer value (as printed by %p in printf(3);
					// the next pointer must be a pointer to a pointer to void.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				case 'n':
					// Nothing is expected; instead, the number of characters
					// consumed thus far from the input is stored through the
					// next pointer, which must be a pointer to int. This is
					// not a conversion and does not increase the count returned
					// by the function. The assignment can be  suppressed with
					// the * assignment-suppression character, but the effect
					// on the return value is undefined. Therefore %*n
					// conversions should not be used.
				{
					fflush(stdout);
					assert(!"TODO");
					break;
				}
				
				default:
				{
					fflush(stdout);
					assert(!"TODO");
					return -1;
				}
			}
			
		}
		// An ordinary character (i.e., one other than white space or '%').
		// This character must exactly match the next character of input.
		else if (c != f)
			break;
		else if ((c = getc(ptr)) == EOF)
			return EOF;
		
		f = *fmt++;
	}
	
	ungetc(c, ptr);
	return count;
}


























