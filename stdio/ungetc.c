
/*#include <assert/assert.h>*/
/*#include <stdio/stdout.h>*/
/*#include <stdio/fflush.h>*/

#include <stdio/EOF.h>

#include "ungetc.h"

int ungetc(int c, FILE *stream)
{
	if (stream->i > 0)
		return stream->buffer[--stream->i] = c;
	else
		return EOF;
}

