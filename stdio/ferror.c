
#include <assert/assert.h>

#include "ferror.h"

int ferror(FILE *stream)
{
	return stream->error_flag;
}
