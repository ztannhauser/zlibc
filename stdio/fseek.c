
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include "FILE.h"

int fseek(FILE *stream, long offset, int whence)
{
	fpos_t newpos;
	
	if (!stream->is_seekable)
		return EOF;
	else if (whence == SEEK_SET
		&& stream->pos == 0
		&& offset < stream->n)
		stream->i = offset;
	else if (whence == SEEK_CUR
		&& 0 <= (newpos = stream->i + offset)
		&& newpos < stream->n)
		stream->i += offset;
	else if (fflush(stream) == EOF)
		return EOF;
	else if ((newpos = lseek(stream->fd, offset, whence)) < 0)
		return EOF;
	else
	{
		stream->pos = newpos;
		stream->i = 0;
		stream->n = 0;
	}
	
	assert(!"TODO");
}

