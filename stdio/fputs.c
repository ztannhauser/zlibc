

#include <string/strlen.h>

#include "fwrite.h"
#include "fputs.h"

int fputs(const char *s, FILE *stream)
{
	return fwrite(s, 1, strlen(s), stream);
}

