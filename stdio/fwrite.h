
#include <stddef/size_t.h>

#include "FILE.h"

size_t   fwrite(const void *restrict, size_t, size_t, FILE *restrict);
