
#include "getc.h"
#include "ungetc.h"
#include "vgscanf.h"
#include "vfscanf.h"

int vfscanf(FILE* stream, const char* fmt, va_list vargs)
{
	return vgscanf(
		stream,
		(int (*)(void*)) getc,
		(int (*)(int, void*)) ungetc,
		fmt, vargs);
}

