
#include <fcntl/O_WRONLY.h>

#include <stdbool/false.h>
#include <stdbool/true.h>

#include "stderr.h"

FILE* stderr = &(struct FILE) {
	.fd = 2,
	.oflags = O_WRONLY,
	.pos = 0,
	.i = 0,
	.n = BUFSIZ,
	.is_seekable = false,
	.is_unbuffered = true,
	.is_allocd = false,
	.error_flag = false,
	.eof_flag = false,
};

