
#ifndef STRUCT_FILE_H
#define STRUCT_FILE_H

#include <stdio/fpos_t.h>
#include <stdio/BUFSIZ.h>

#include <stdbool/bool.h>

struct FILE
{
	int fd;
	int oflags;
	
	fpos_t pos;
	
	unsigned i, n;
	char buffer[BUFSIZ];
	
	bool error_flag, eof_flag;
	
	bool is_seekable;
	
	// only when unbuffered, seekable and opened from writing:
	bool is_dirty;
	
	bool is_unbuffered;
	
	bool is_allocd;
};

typedef struct FILE FILE;

#endif
