
#include <stdarg/va_list.h>

int      vsprintf(char *restrict, const char *restrict, va_list);
