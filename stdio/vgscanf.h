
#include <stdarg/va_list.h>

int vgscanf(
	void* ptr,
	int (*getc)(void* ptr),
	int (*ungetc)(int c, void* ptr),
	const char* fmt, va_list vargs);
