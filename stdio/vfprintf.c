
#include "fwrite.h"
#include "vgprintf.h"
#include "fprintf.h"

int vfprintf(FILE *stream, const char *fmt, va_list vargs)
{
	return vgprintf(
		stream,
		(size_t (*)(const void * restrict,  size_t,  size_t,  void* restrict)) fwrite,
		fmt, vargs);
}
























