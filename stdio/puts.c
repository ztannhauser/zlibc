
#include <stdio/stdout.h>
#include <stdio/fputs.h>
#include <stdio/fputc.h>
#include <stdio/puts.h>
#include <stdio/EOF.h>

int puts(const char *s)
{
	int retval;
	
	if (fputs(s, stdout) == EOF || fputc('\n', stdout) == EOF)
		retval = EOF;
	else
		retval = 1;
	
	return retval;
}

