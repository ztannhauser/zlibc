
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "FILE.h"

int fclose(FILE *stream)
{
	int retval = 0;
	
	if (fflush(stream) || close(stream->fd) < 0)
		retval = EOF;
	
	if (stream->is_allocd)
		free(stream);
	
	return retval;
}
















