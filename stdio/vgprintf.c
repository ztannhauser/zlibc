
#include <assert/assert.h>

#include <errno/errno.h>

#include <stddef/ptrdiff_t.h>

#include <stdbool/true.h>
#include <stdbool/false.h>
#include <stdbool/bool.h>

#include <string/strnlen.h>
#include <string/strlen.h>
#include <string/strerror.h>

#include <strings/index.h>

#include <stdarg/va_arg.h>

#include <stdint/intmax_t.h>
#include <stdint/uintmax_t.h>

#include <sys/types/size_t.h>
#include <sys/types/ssize_t.h>

#include <sys/param/MAX.h>

#ifdef DEBUGGING
#include <unistd/write.h>
#endif

#define BAD_FORMAT (-2)

#include "EOF.h"
#include "vgprintf.h"

int vgprintf(
	void* stream,
	size_t (*fwrite)(const void* restrict, size_t, size_t, void* restrict),
	const char *fmt, va_list vargs)
{
	int retval = 0;
	char f;
	
	while (retval >= 0 && (f = *fmt++))
	{
		if (f == '%')
		{
			struct {
				bool pound, zero, minus, space, plus;
			} flags = {};
			
			while ((f = *fmt++) && index("#0- +", f))
				switch (f)
				{
					case '#': flags.pound = true; break;
					case '0': flags.zero  = true; break;
					case '-': flags.minus = true; break;
					case ' ': flags.space = true; break;
					case '+': flags.plus  = true; break;
				}
			
			struct {
				bool is_set;
				unsigned int val;
			} field_width = {}, digits_of_precision = {};
			
			if (!f);
			else if (index("0123456789", f))
			{
				field_width.is_set = true, field_width.val = 0;
				
				do field_width.val = field_width.val * 10 + f - '0';
				while ((f = *fmt++) && index("0123456789", f));
			}
			else if (f == '*')
			{
				f = *fmt++;
				
				field_width.is_set = true,
				field_width.val = va_arg(vargs, int);
			}
			
			if (f == '.' && (f = *fmt++))
			{
				if (index("0123456789", f))
				{
					digits_of_precision.is_set = true, digits_of_precision.val = 0;
					
					do digits_of_precision.val = digits_of_precision.val * 10 + f - '0';
					while ((f = *fmt++) && index("0123456789", f));
				}
				else if (f == '*')
				{
					f = *fmt++;
					
					digits_of_precision.is_set = true,
					digits_of_precision.val = va_arg(vargs, int);
				}
			}
			
			enum length_modifier {
				lm_char, lm_short, lm_int, lm_long, lm_long_long,
				lm_long_double, lm_intmax, lm_size, lm_ptrdiff
			} lm = lm_int;
			
			switch (f)
			{
				case 'h':
					if ((f = *fmt++) == 'h')
						f = *fmt++, lm = lm_char;
					else
						lm = lm_short;
					break;
				
				case 'l':
					if ((f = *fmt++) == 'l')
						f = *fmt++, lm = lm_long_long;
					else
						lm = lm_long;
					break;
				
				case 'q': f = *fmt++, lm = lm_long_long; break;
				
				case 'L': f = *fmt++, lm = lm_long_double; break;
				
				case 'j': f = *fmt++, lm = lm_intmax; break;
				
				case 'z': f = *fmt++, lm = lm_size; break;
				
				case 't': f = *fmt++, lm = lm_ptrdiff; break;
			}
			
			switch (f)
			{
				case L'i':
				case L'd':
					// The int argument is converted to signed decimal
					// notation. The precision, if any, gives the minimum
					// number of digits that must appear; if the converted
					// value requires fewer digits, it is padded on the left
					// with zeros. The default precision is 1. When 0 is
					// printed with an explicit precision 0, the output is
					// empty.
				{
					char digits[30 + 1];
					char* e = &digits[30 + 1], *m = e;
					intmax_t val;
					
					switch (lm)
					{
						case lm_char:      val = va_arg(vargs, signed       int); break;
						case lm_short:     val = va_arg(vargs, signed       int); break;
						case lm_int:       val = va_arg(vargs, signed       int); break;
						case lm_long:      val = va_arg(vargs, signed      long); break;
						case lm_long_long: val = va_arg(vargs, signed long long); break;
						
						case lm_intmax:  val = va_arg(vargs,   intmax_t); break;
						case lm_size:    val = va_arg(vargs,    ssize_t); break;
						case lm_ptrdiff: val = va_arg(vargs,  ptrdiff_t); break;
						
						default: val = 0, retval = BAD_FORMAT; break;
					}
					
					bool negative = false;
					
					if (val < 0)
					{
						negative = true;
						val = -val;
					}
					
					while (val)
					{
						*--m = "0123456789"[val % 10];
						val /= 10;
					}
					
					if (negative)
						*--m = '-';
					
					unsigned precision = digits_of_precision.is_set
						? digits_of_precision.val : 1;
					
					size_t len = e - m;
					
					size_t max = MAX(precision, len);
					
					if (field_width.is_set)
						while (field_width.val-- > max)
							if (fwrite(" ", 1, 1, stream) == EOF)
								return EOF;
					
					while (precision-- > len)
						if (fwrite("0", 1, 1, stream) == EOF)
							return EOF;
					
					if (fwrite(m, 1, len, stream) < len)
						return EOF;
					
					break;
				}
				
				case 'b':
				case 'o':
				case 'u':
				case 'x':
				case 'X':
					// The unsigned int argument is converted to unsigned
					// octal (o), unsigned decimal (u), or unsigned
					// hexadecimal (x and X) notation. The letters abcdef
					// are used for x conversions; the letters ABCDEF are
					// used for X conversions. The precision, if any, gives
					// the minimum  number of digits that must appear; if
					// the converted value requires fewer digits, it is
					// padded on the left with zeros. The default precision
					// is 1. When 0 is printed with an explicit precision 0,
					// the output is empty.
				{
					char digits[20 + 1];
					char* e = &digits[20 + 1], *m = e;
					uintmax_t val;
					unsigned base = f == 'o' ? 8
					              : f == 'u' ? 10
					              : f == 'b' ? 2
					              : 16;
					
					switch (lm)
					{
						case lm_char:      val = va_arg(vargs, signed       int); break;
						case lm_short:     val = va_arg(vargs, signed       int); break;
						case lm_int:       val = va_arg(vargs, signed       int); break;
						case lm_long:      val = va_arg(vargs, signed      long); break;
						case lm_long_long: val = va_arg(vargs, signed long long); break;
						
						case lm_intmax:  val = va_arg(vargs,   intmax_t); break;
						case lm_size:    val = va_arg(vargs,     size_t); break;
						case lm_ptrdiff: val = va_arg(vargs,  ptrdiff_t); break;
						
						default: val = 0, retval = -2; break;
					}
					
					if (!val)
						*--m = '0';
					else while (val) {
						*--m = "0123456789ABCDEF"[val % base];
						val /= base;
					}
					
					if (base != 10 && flags.pound)
					{
						if (base == 16)
							*--m = 'x';
						*--m = '0';
					}
					
					size_t len = e - m;
					
					if (field_width.is_set)
						while (field_width.val-- > len)
						{
							if (fwrite(" ", 1, 1, stream) == EOF)
								return EOF;
						}
					
					if (fwrite(m, 1, len, stream) < len)
						return EOF;
					else
						retval += len;
					
					break;
				}
				
				// above were all the integer converisions.
				
				case 'e':
				// case 'E':
					// The double argument is rounded and converted in the
					// style [-]d.ddde±dd where there is one digit before
					// the decimal-point character and the number of digits
					// after it is equal to the precision; if the precision
					// is missing, it is taken as 6; if the precision is
					// zero, no decimal-point character appears. An E
					// conversion uses the letter E (rather than e) to
					// introduce the exponent. The exponent always contains
					// at least two digits; if the value is zero, the
					// exponent is 00.
				case 'f':
				case 'F':
					// The double argument is rounded and converted to
					// decimal notation in the style [-]ddd.ddd, where the
					// number of digits after the decimal-point character is
					// equal to the precision specification. If the
					// precision is missing, it is taken as 6; if the
					// precision is explicitly zero, no decimal-point
					// character appears. If a decimal point appears, at
					// least one digit appears before it.
					
					// (SUSv2 does not know about F and says that character
					// string representations for infinity and NaN may be
					// made available. SUSv3 adds a specification for F. The
					// C99 standard specifies "[-]inf" or "[-]infinity" for
					// infinity, and a string starting with "nan" for NaN,
					// in the case of f conversion, and "[-]INF" or
					// "[-]INFINITY" or "NAN" in the case of F conversion.)
				case 'g':
				case 'G':
					// The double argument is converted in style f or e
					// (or F or E for G conversions). The precision
					// specifies the number of significant digits. If the
					// precision is missing, 6 digits are given; if the
					// precision is zero, it is treated as 1. Style e is
					// used if the exponent from its conversion is less
					// than -4 or greater than or equal to the precision.
					// Trailing zeros are removed from the fractional part
					// of the result; a decimal point appears only if it is
					// followed by at least one digit.
				case 'a':
				case 'A':
					// (C99; not in SUSv2, but added in SUSv3) For a
					// conversion, the double argument is converted to
					// hexadecimal notation (using the letters abcdef) in
					// the style [-]0xh.hhhhp±; for A conversion the prefix
					// 0X, the letters ABCDEF, and the exponent separator P
					// is used. There is one hexadecimal digit before the
					// decimal point, and the number of digits after it is
					// equal to the precision. The default precision
					// suffices for an exact representation of the value if
					// an exact representation in base 2 exists and
					// otherwise is sufficiently large to distinguish values
					// of type double. The digit before the decimal point is
					// unspecified for nonnormalized numbers, and nonzero
					// but otherwise unspecified for normalized numbers.
				{
					assert(!"TODO 207");
					break;
				}
				
				case 'c':
					// If no l modifier is present, the int argument is
					// converted to an unsigned char, and the resulting
					// character is written. If an l modifier is present,
					// the wint_t (wide character) argument is converted to
					// a multibyte sequence by a call to the wcrtomb(3)
					// function, with a conversion state starting in the
					// initial state, and the resulting multibyte string
					// is written.
				{
					switch (lm)
					{
						case lm_int:
						{
							char c = va_arg(vargs, int);
							
							if (fwrite(&c, 1, 1, stream) == EOF)
								retval = EOF;
							else
								retval += 1;
							
							break;
						}
						
						case lm_long:
							assert(!"TODO 272");
							break;
						
						default:
							retval = -2;
							break;
					}
					break;
				}
				
				case 's':
					// If no l modifier is present: the const char *
					// argument is expected to be a pointer to an array of
					// character type (pointer to a string). Characters from
					// the array are written up to (but not including) a
					// terminating null byte ('\0'); if a precision is
					// specified, no more than the number specified are
					// written. If a precision is given, no null byte need
					// be present; if the precision is not specified, or
					// is greater than the size of the array, the array must
					// contain a terminating null byte.
					
					// If an l modifier is present: the const wchar_t *
					// argument is expected to be a pointer to an array of
					// wide characters. Wide characters from the array are
					// converted to multibyte characters (each by a call to
					// the wcrtomb(3) function, with a conversion state
					// starting in the initial state before the first wide
					// character), up to and including a terminating null
					// wide character. The resulting multibyte characters
					// are written up to (but not including) the terminating
					// null byte. If a precision is specified, no more bytes
					// than the number specified are written, but no partial
					// multibyte characters are written. Note that the
					// precision determines the number of bytes written, not
					// the number of wide characters or screen positions.
					// The array must contain a terminating null wide
					// character, unless a precision is given and it is so
					// small that the number of bytes written exceeds it
					// before the end of the array is reached.
				{
					switch (lm)
					{
						case lm_int:
						{
							char* arg = va_arg(vargs, char*);
							
							if (arg)
							{
								size_t len = digits_of_precision.is_set
									? strnlen(arg, digits_of_precision.val)
									: strlen(arg);
								
								if (field_width.is_set)
									while (field_width.val-- > len)
										if (fwrite(" ", 1, 1, stream) == EOF)
											return EOF;
								
								if (fwrite(arg, 1, len, stream) < len)
									return EOF;
							}
							else if (fwrite("(null)", 1, 7, stream) == EOF)
								return EOF;
							
							break;
						}
						
						case lm_long:
							assert(!"TODO 272");
							break;
						
						default:
							retval = BAD_FORMAT;
							break;
					}
					break;
				}
				
				case 'p':
					// The void * pointer argument is printed in hexadecimal
					// (as if by %#x or %#lx).
				{
					char digits[2 + 16 + 1];
					char* e = &digits[2 + 16 + 1], *m = e;
					uintmax_t val = (uintmax_t) va_arg(vargs, void*);
					
					if (!val)
						*--m = '0';
					else while (val) {
						*--m = "0123456789abcdef"[val & 0xF];
						val >>= 4;
					}
					
					*--m = 'x';
					*--m = '0';
					
					size_t len = e - m;
					
					if (field_width.is_set)
						while (field_width.val-- > len)
							if (fwrite(" ", 1, 1, stream) == EOF)
								return EOF;
					
					if (fwrite(m, 1, len, stream) < len)
						return EOF;
					else
						retval += len;
					
					break;
				}
				
				case 'm':
					// (Glibc extension; supported by uClibc and musl.) 
					// Print output of strerror(errno). No argument is required.
				{
					char* arg = strerror(errno);
				
					size_t len = digits_of_precision.is_set
						? strnlen(arg, digits_of_precision.val)
						: strlen(arg);
					
					if (field_width.is_set)
						while (field_width.val-- > len)
							if (fwrite(" ", 1, 1, stream) == EOF)
								return EOF;
					
					if (fwrite(arg, 1, len, stream) < len)
						return EOF;
					
					break;
				}
				
				case '%':
					if (fwrite("%", 1, 1, stream) == EOF)
						return EOF;
					retval++;
					break;
				
				default:
					// incomplete format string!
					retval = BAD_FORMAT;
					break;
			}
		}
		else if (fwrite(&f, 1, 1, stream) == EOF)
			return EOF;
		else
			retval++;
	}
	
	return retval;
}
























