
#define WRITE_MAX 0x7ffff000

#include <fcntl/O_ACCMODE.h>
#include <fcntl/O_RDONLY.h>
#include <fcntl/O_APPEND.h>
#include <fcntl/O_RDWR.h>

#include <sys/param/MIN.h>

#include <stdio/EOF.h>

#include <stdbool/true.h>

#include <string/memcpy.h>

#include <unistd/read.h>
#include <unistd/pwrite.h>
#include <unistd/write.h>

#include "fwrite.h"

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	ssize_t a, b = BUFSIZ, c = 0;
	size_t len = size * nmemb;
	
	if ((stream->oflags & O_ACCMODE) == O_RDONLY)
	{
		// not opened for writing
		return EOF;
	}
	else if (stream->is_unbuffered || (stream->oflags & O_APPEND))
	{
		while (len)
		{
			a = MIN(len, WRITE_MAX);
			
			if ((b = write(stream->fd, ptr, len)) < 0)
			{
				stream->error_flag = true;
				break;
			}
			else if (!b)
			{
				stream->eof_flag = true;
				break;
			}
			else
			{
				if (!(stream->oflags & O_APPEND))
					stream->pos += b;
				c += b, ptr += b, len -= b;
			}
		}
	}
	else while (len)
	{
		stream->is_dirty = true;
		a = MIN(len, stream->n - stream->i);
		memcpy(stream->buffer + stream->i, ptr, a);
		stream->i += a, c += a, ptr += a, len -= a;
		
		if (!len);
		else if (stream->is_seekable
			? pwrite(stream->fd, stream->buffer, stream->n, stream->pos) < 0
			:  write(stream->fd, stream->buffer, stream->n             ) < 0)
		{
			stream->error_flag = true;
			return c;
		}
		else if (true
			&& (stream->oflags & O_ACCMODE) == O_RDWR
			&& (b = read(stream->fd, stream->buffer, BUFSIZ)) <= 0)
		{
			if (b)
				stream->error_flag = true;
			else
				stream->eof_flag = true;
			
			return c;
		}
		else
		{
			stream->pos += stream->n;
			stream->n = b;
			stream->i = 0;
		}
	}
	
	return c;
}
















