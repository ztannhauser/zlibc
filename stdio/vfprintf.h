
#include <stdio/FILE.h>

#include <stdarg/va_list.h>

int      vfprintf(FILE *restrict, const char *restrict, va_list);
