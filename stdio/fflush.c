
#include <stdbool/true.h>
#include <stdbool/false.h>

#include <sys/types/ssize_t.h>

#include <unistd/pwrite.h>
#include <unistd/write.h>

#include "stderr.h"
#include "fprintf.h"
#include "EOF.h"
#include "fflush.h"

int fflush(FILE *stream)
{
	ssize_t r;
	
	if (!stream->is_dirty || stream->is_unbuffered)
		;
	else if (stream->is_seekable
		? (r = pwrite(stream->fd, stream->buffer, stream->i, stream->pos)) < 0
		: (r =  write(stream->fd, stream->buffer, stream->i)) < 0)
	{
		stream->error_flag = true;
		return EOF;
	}
	else
	{
		stream->pos += stream->n;
		stream->i = 0;
		stream->n = 0;
		stream->is_dirty = false;
	}
	
	return 0;
}












