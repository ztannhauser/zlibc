
#include <assert.h>
#include <stdio.h>

int getc(FILE *stream)
{
	char c;
	
	if (fread(&c, 1, 1, stream) == EOF)
		return EOF;
	
	return c;
}

