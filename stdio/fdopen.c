
#include <fcntl/O_CREAT.h>
#include <fcntl/O_TRUNC.h>
#include <fcntl/O_APPEND.h>
#include <fcntl/O_RDWR.h>
#include <fcntl/O_RDONLY.h>

#include <sys/stat/S_ISDIR.h>
#include <sys/stat/S_ISLNK.h>
#include <sys/stat/S_ISREG.h>
#include <sys/stat/stat.h>
#include <sys/stat/fstat.h>

#include <fcntl/O_ACCMODE.h>
#include <fcntl/F_GETFL.h>
#include <fcntl/fcntl.h>

#include <stddef/NULL.h>

#include <stdbool/true.h>
#include <stdbool/false.h>

#include <stdlib/malloc.h>

#include "fdopen.h"

FILE *fdopen(int fd, const char *mode)
{
	int oflags = 0;
	
	switch (*mode++)
	{
		case 'r': oflags |= 0; break;
		case 'w': oflags |= O_CREAT | O_TRUNC; break;
		case 'a': oflags |= O_CREAT | O_APPEND; break;
		default: return NULL;
	}
	
	if (*mode == '+')
		mode++, oflags |= O_RDWR;
	else
		oflags |= O_RDONLY;
	
	struct stat sb;
	FILE* retval = NULL;
	
	if (true
		// not silly file type?
		&& !fstat(fd, &sb) && !S_ISDIR(sb.st_mode) && !S_ISLNK(sb.st_mode)
		// is given access mode possible with given fd?
		&& (fcntl(fd, F_GETFL) & oflags) == oflags
		// if it's a stream: opened in only one direction?
		&& (S_ISREG(sb.st_mode) >= ((oflags & O_ACCMODE) == O_RDWR))
		// malloc didn't fail?
		&& (retval = malloc(sizeof(FILE))))
	{
		retval->fd = fd;
		retval->oflags = oflags;
		retval->pos = 0;
		retval->i = 0;
		retval->n = 0;
		
		retval->is_seekable = S_ISREG(sb.st_mode);
		retval->is_unbuffered = false;
		retval->is_dirty = false;
		retval->error_flag = false;
		retval->eof_flag = false;
		retval->is_allocd = true;
	}
	
	return retval;
}












