
#include <fcntl/O_WRONLY.h>

#include <stdbool/false.h>
#include <stdbool/true.h>

#include "stdout.h"

FILE* stdout = &(struct FILE) {
	.fd = 1,
	.oflags = O_WRONLY,
	.pos = 0,
	.i = 0,
	.n = BUFSIZ,
	.is_seekable = false,
	.is_unbuffered = false,
	.is_allocd = false,
	.error_flag = false,
	.eof_flag = false,
};

