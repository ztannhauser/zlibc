
#include <stddef/size_t.h>

/*#include <stdio/fflush.h>*/
/*#include <stdio/stdout.h>*/

/*#include <assert/assert.h>*/

#include <string/mempcpy.h>

#include "vgprintf.h"
#include "vsprintf.h"

static size_t myfwrite(const void* restrict data, size_t n, size_t m, void* restrict ptr)
{
	char** s = ptr;
	size_t len = n * m;
	*s = mempcpy(*s, data, len);
	return len;
}

int vsprintf(char *str, const char *fmt, va_list vargs)
{
	int retval = vgprintf(&str, myfwrite, fmt, vargs);
	
	if (retval >= 0)
		*str = '\0';
	
	return retval;
}

