
#include <stddef/NULL.h>
#include <stdbool/true.h>
#include <stdbool/false.h>
#include <sys/stat/stat.h>
#include <sys/stat/fstat.h>
#include <sys/stat/S_ISDIR.h>
#include <sys/stat/S_ISLNK.h>
#include <sys/stat/S_ISREG.h>
#include <fcntl/O_CREAT.h>
#include <fcntl/O_TRUNC.h>
#include <fcntl/O_APPEND.h>
#include <fcntl/O_RDWR.h>
#include <fcntl/O_RDONLY.h>
#include <fcntl/O_ACCMODE.h>
#include <fcntl/open.h>
#include <stdlib/malloc.h>
#include <unistd/close.h>

#include "FILE.h"

FILE* fopen(const char *pathname, const char *mode)
{
	int oflags = 0;
	struct stat sb;
	FILE* retval;
	int fd;
	
	switch (*mode++)
	{
		case 'r': oflags |= 0; break;
		case 'w': oflags |= O_CREAT | O_TRUNC; break;
		case 'a': oflags |= O_CREAT | O_APPEND; break;
		default: return NULL;
	}
	
	if (*mode == '+')
		mode++, oflags |= O_RDWR;
	else
		oflags |= O_RDONLY;
	
	if (true
		// open didn't fail?
		&& (fd = open(pathname, oflags, 0664)) > 0
		// not silly file type?
		&& !fstat(fd, &sb) && !S_ISDIR(sb.st_mode) && !S_ISLNK(sb.st_mode)
		// if it's a stream: opened in only one direction?
		&& (S_ISREG(sb.st_mode) >= ((oflags & O_ACCMODE) == O_RDWR))
		// malloc didn't fail?
		&& (retval = malloc(sizeof(FILE))))
	{
		retval->fd = fd, fd = -1;
		retval->oflags = oflags;
		retval->pos = 0;
		retval->i = 0;
		retval->n = 0;
		
		retval->is_seekable = S_ISREG(sb.st_mode);
		retval->is_unbuffered = false;
		retval->is_dirty = false;
		retval->error_flag = false;
		retval->eof_flag = false;
		retval->is_allocd = true;
	}
	
	if (fd > 0)
		close(fd);
	
	return retval;
}






















