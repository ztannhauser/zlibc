
#include <stddef/size_t.h>

#include <string/memcpy.h>

#include <sys/param/MIN.h>

#include "vgprintf.h"
#include "vsprintf.h"

struct bundle
{
	char* s;
	size_t n;
};

static size_t myfwrite(const void* restrict data, size_t n, size_t m, void* restrict ptr)
{
	struct bundle* b = ptr;
	size_t expected = n * m;
	size_t actual   = MIN(b->n, expected);
	
	memcpy(b->s, data, actual);
	b->s += actual, b->n -= actual;
	
	return expected;
}

int vsnprintf(char *s, size_t n, const char *fmt, va_list vargs)
{
	struct bundle b = {.s = s, .n = n - 1};
	
	int retval = vgprintf(&b, myfwrite, fmt, vargs);
	
	if (retval >= 0)
	{
		s[retval] = '\0';
	}
	
	return retval;
}

