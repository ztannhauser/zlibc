
#include <stddef/size_t.h>
#include <stdarg/va_list.h>

int      vsnprintf(char *restrict, size_t, const char *restrict, va_list);
