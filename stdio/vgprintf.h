
#include <stddef/size_t.h>

#include <stdarg/va_list.h>

int vgprintf(
	void* stream,
	size_t (*fwrite)(const void* restrict, size_t, size_t, void* restrict),
	const char *fmt, va_list vargs);
