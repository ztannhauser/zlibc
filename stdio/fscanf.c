
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

int fscanf(FILE *stream, const char *format, ...)
{
	va_list vargs;
	
	va_start(vargs, format);
	
	int retval = vfscanf(stream, format, vargs);
	
	va_end(vargs);
	
	return retval;
}
