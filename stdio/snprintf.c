
#include <stdarg/va_start.h>
#include <stdarg/va_end.h>

#include "vsnprintf.h"
#include "snprintf.h"

int snprintf(char *str, size_t n, const char *fmt, ...)
{
	int retval;
	va_list vargs;
	
	va_start(vargs, fmt);
	
	retval = vsnprintf(str, n, fmt, vargs);
	
	va_end(vargs);
	
	return retval;
}

