
#include "stdio.h"
#include "vfprintf.h"
#include "vprintf.h"

int vprintf(const char *format, va_list ap)
{
	return vfprintf(stdout, format, ap);
}

