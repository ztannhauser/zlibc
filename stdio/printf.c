
#include <stdio.h>
#include <stdarg.h>

int printf(const char* format, ...)
{
	int retval;
	
	va_list ap;
	
	va_start(ap, format);
	
	retval = vprintf(format, ap);
	
	va_end(ap);
	
	return retval;
}

