
#ifndef TIME_H
#define TIME_H

// The <time.h> header shall define the clock_t, size_t, time_t, types as
// described in <sys/types.h>.
#include <sys/types/clock_t.h>
#include <sys/types/size_t.h>
#include <sys/types/time_t.h>

// The <time.h> header shall define the clockid_t and timer_t types as
// described in <sys/types.h>:
#include <sys/types/clockid_t.h>
#include <sys/types/timer_t.h>

// The <time.h> header shall define the locale_t type as described in <locale.h>.
#include <locale/locale_t.h>

// The <time.h> header shall define the pid_t type as described in <sys/types.h>:
#include <sys/types/pid_t.h>

// The tag sigevent shall be declared as naming an incomplete structure type,
// the contents of which are described in the <signal.h> header:
struct sigevent;

// the <time.h> header shall declare the tm structure:
#include <time/tm.h>

// The value of tm_isdst shall be positive if Daylight Savings Time is in
// effect, 0 if Daylight Savings Time is not in effect, and negative if the
// information is not available.

// The <time.h> header shall declare the timespec structure:
#include <time/timespec.h>

// The <time.h> header shall also declare the itimerspec structure:
#include <time/itimespec.h>

// The <time.h> header shall define the following macros:

// NULL
	// as described in <stddef.h>:
#include <stddef/NULL.h>

// CLOCKS_PER_SEC
	// A  number  used  to convert the value returned by the clock() function
	// into seconds. The value shall be an expression with type clock_t.  The
	// value of CLOCKS_PER_SEC shall be 1 million on XSI-conformant systems.
	// However, it may  be  variable  on other systems, and it should not be
	// assumed that CLOCKS_PER_SEC is a compile-time constant.
#include <time/CLOCKS_PER_SEC.h>

// The  <time.h>  header  shall  define  the  following symbolic constants. The
// values shall have a type that is assignment-compatible with clockid_t.

// CLOCK_MONOTONIC
	// The identifier for the system-wide monotonic clock, which is defined as
	// a clock measuring real time, whose value cannot be set via
	// clock_settime() and which cannot have negative clock jumps. The maximum
	// possible clock jump shall be implementation-defined.
#include <time/CLOCK_MONOTONIC.h>

// CLOCK_PROCESS_CPUTIME_ID
	// The identifier of the CPU-time clock associated with the process making
	// a clock() or timer*() function call.
#include <time/CLOCK_PROCESS_CPUTIME_ID.h>

// CLOCK_REALTIME
	// The identifier of the system-wide clock measuring real time.
#include <time/CLOCK_REALTIME.h>

// CLOCK_THREAD_CPUTIME_ID
	// The identifier of the CPU-time clock associated with the thread making a
	// clock() or timer*() function call.
#include <time/CLOCK_THREAD_CPUTIME_ID.h>

// The <time.h> header shall define the following symbolic constant:

// TIMER_ABSTIME
	// Flag indicating time is absolute. For functions taking timer
	// objects, this refers to the clock associated with the timer.
#include <time/TIMER_ABSTIME.h>

// The <time.h> header shall provide a declaration or definition for
// getdate_err. The getdate_err symbol shall expand to an expression of type
// int. It is unspecified whether getdate_err is a macro or an identifier
// declared with external linkage, and whether or not it is a modifiable lvalue.
// If a macro definition is suppressed in order to access an actual object, or
// a program defines an identifier with the name getdate_err, the behavior is
// undefined.

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <time/asctime.h>
#include <time/asctime_r.h>
#include <time/clock.h>
#include <time/clock_getcpuclockid.h>
#include <time/clock_getres.h>
#include <time/clock_gettime.h>
#include <time/clock_nanosleep.h>
#include <time/clock_settime.h>
#include <time/ctime.h>
#include <time/ctime_r.h>
#include <time/difftime.h>
#include <time/getdate.h>
#include <time/gmtime.h>
#include <time/gmtime_r.h>
#include <time/localtime.h>
#include <time/localtime_r.h>
#include <time/mktime.h>
#include <time/nanosleep.h>
#include <time/strftime.h>
#include <time/strftime_l.h>
#include <time/strptime.h>
#include <time/time.h>
#include <time/timer_create.h>
#include <time/timer_delete.h>
#include <time/timer_getoverrun.h>
#include <time/timer_gettime.h>
#include <time/timer_settime.h>
#include <time/tzset.h>

// The <time.h> header shall declare the following as variables:

#include <time/daylight.h>
#include <time/timezone.h>
#include <time/tzname.h>

// Inclusion of the <time.h> header may make visible all symbols from the
// <signal.h> header:
// #include <signal.h>
















#endif
