
#ifdef MULTITHREADED

#include <pthread/attr_destroy.h>
#include <pthread/attr_getaffinity_np.h>
#include <pthread/attr_getdetachstate.h>
#include <pthread/attr_getguardsize.h>
#include <pthread/attr_getinheritsched.h>
#include <pthread/attr_getschedparam.h>
#include <pthread/attr_getschedpolicy.h>
#include <pthread/attr_getscope.h>
#include <pthread/attr_getstackaddr.h>
#include <pthread/attr_getstack.h>
#include <pthread/attr_getstacksize.h>
#include <pthread/attr_init.h>
#include <pthread/attr_setaffinity_np.h>
#include <pthread/attr_setdetachstate.h>
#include <pthread/attr_setguardsize.h>
#include <pthread/attr_setinheritsched.h>
#include <pthread/attr_setschedparam.h>
#include <pthread/attr_setschedpolicy.h>
#include <pthread/attr_setscope.h>
#include <pthread/attr_setstackaddr.h>
#include <pthread/attr_setstack.h>
#include <pthread/attr_setstacksize.h>
#include <pthread/cancel.h>
#include <pthread/clockjoin_np.h>
#include <pthread/create.h>
#include <pthread/detach.h>
#include <pthread/equal.h>
#include <pthread/exit.h>
#include <pthread/getaffinity_np.h>
#include <pthread/getattr_default_np.h>
#include <pthread/getattr_np.h>
#include <pthread/getname_np.h>
#include <pthread/getschedparam.h>
#include <pthread/join.h>
#include <pthread/once.h>
#include <pthread/self.h>
#include <pthread/setaffinity_np.h>
#include <pthread/setattr_default_np.h>
#include <pthread/setcancelstate.h>
#include <pthread/setcanceltype.h>
#include <pthread/setname_np.h>
#include <pthread/setschedparam.h>
#include <pthread/setschedprio.h>
#include <pthread/testcancel.h>
#include <pthread/timedjoin_np.h>
#include <pthread/tryjoin_np.h>
#include <pthread/yield.h>

#endif

