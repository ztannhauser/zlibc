
#ifndef STRUCT_PASSWD_H
#define STRUCT_PASSWD_H

#include <sys/types/uid_t.h>
#include <sys/types/gid_t.h>

struct passwd {
	char   *pw_name;       /* username */
	char   *pw_passwd;     /* user password */
	uid_t   pw_uid;        /* user ID */
	gid_t   pw_gid;        /* group ID */
	char   *pw_gecos;      /* user information */
	char   *pw_dir;        /* home directory */
	char   *pw_shell;      /* shell program */
};

#endif
