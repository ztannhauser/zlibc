
/*#include <sys/types/uid_t.h>*/
/*#include <sys/types/gid_t.h>*/

#include <limits/PATH_MAX.h>

#include <stdbool/false.h>

#include <stdio/FILE.h>
#include <stdio/fopen.h>
#include <stdio/fscanf.h>
#include <stdio/fclose.h>

#include <stdlib/bsearch.h>
#include <stdlib/malloc.h>
#include <stdlib/realloc.h>
#include <stdlib/free.h>
#include <stdlib/atexit.h>

#include <string/strdup.h>

#include <stddef/NULL.h>

#include <assert/assert.h>

#include "getpwuid.h"
#include "passwd.h"

static struct {
	struct passwd** data;
	unsigned n, cap;
} cache = {NULL, 0, 0};

static int compar(const void* a, const void* b)
{
	const uid_t* A = a;
	const struct passwd* const * B = b;
	
	if (*A < (*B)->pw_uid)
		return -1;
	else if (*A > (*B)->pw_uid)
		return 1;
	else
		return 0;
}

void free_cache()
{
	while (cache.n--)
	{
		struct passwd* P = cache.data[cache.n];
		
		free(P->pw_name);       /* username */
		free(P->pw_passwd);     /* user password */
		free(P->pw_gecos);      /* user information */
		free(P->pw_dir);        /* home directory */
		free(P->pw_shell);      /* shell program */
		
		free(P);
	}
	
	free(cache.data);
}

struct passwd* getpwuid(uid_t uid)
{
	// try from cache:
	if (cache.n)
	{
		struct passwd** retval;
		
		retval = bsearch(&uid, cache.data, cache.n, sizeof(*cache.data), compar);
		
		if (retval)
			return *retval;
	}
	
	// otherwise, try from /etc/passwd:
	{
		char  pw_name[32 + 1];    /* username */
		char  pw_passwd[256];     /* user password */
		uid_t pw_uid;             /* user ID */
		gid_t pw_gid;             /* group ID */
		char  pw_gecos[PATH_MAX]; /* user information */
		char  pw_dir[PATH_MAX];   /* home directory */
		char  pw_shell[PATH_MAX]; /* shell program */
		
		// read until you find it:
		{
			FILE* file = NULL;
			int r;
			
			if (!(file = fopen("/etc/passwd", "r")))
				return NULL;
			else while ((r = fscanf(file,
				"%32[^:]:%256[^:]:%u:%u:%4096[^:]:%4096[^:]:%4096[^\n]\n",
				pw_name, pw_passwd, &pw_uid, &pw_gid, pw_gecos,
				pw_dir, pw_shell)) == 7)
			{
				if (pw_uid == uid)
					break;
			}
			
			fclose(file);
			
			if (r < 0)
				return NULL;
		}
		
		// allocate a new struct for it
		struct passwd* p = malloc(sizeof(*p));
		
		// check for out of memory:
		if (false
			|| !p
			|| !(p->pw_name = strdup(pw_name))
			|| !(p->pw_passwd = strdup(pw_passwd))
			|| !(p->pw_gecos = strdup(pw_gecos))
			|| !(p->pw_dir = strdup(pw_dir))
			|| !(p->pw_shell = strdup(pw_shell)))
		{
			if (p) {
				if (p->pw_name) { free(p->pw_name);
				if (p->pw_passwd) { free(p->pw_passwd);
				if (p->pw_gecos) { free(p->pw_gecos);
				if (p->pw_dir) { free(p->pw_dir); } } } }
			}
			return NULL;
		}
		
		p->pw_uid = pw_uid;
		p->pw_gid = pw_gid;
		
		// ensure there's enough space for another in the cache:
		if (cache.n + 1 >= cache.cap)
		{
			unsigned newcap = cache.cap * 2 ?: 1;
			void * new = realloc(cache.data, newcap * sizeof(*cache.data));
			
			if (!cache.data)
				atexit(free_cache);
			
			if (!new)
				return NULL;
			
			cache.cap = newcap;
			cache.data = new;
		}
		
		// insert into cache:
		{
			unsigned i;
			
			for (i = cache.n; i >= 1 && compar(&uid, &cache.data[i - 1]) < 0; i--)
				cache.data[i] = cache.data[i - 1];
			
			cache.n++, cache.data[i] = p;
		}
		
		return p;
	}
	
	return NULL;
}
























