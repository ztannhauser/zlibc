
#ifndef STDLIB_H
#define STDLIB_H

// Some of the functionality described on this reference page extends the ISO
// C standard. Applications shall define the appropriate feature test macro
// (see the System Interfaces volume of POSIX.1‐2008, Section 2.2, The
// Compilation Environment) to enable the visibility of these symbols in this
// header.

// The <stdlib.h> header shall define the following macros which shall expand
// to integer constant expressions:

// EXIT_FAILURE
	// Unsuccessful termination for exit(); evaluates to a non-zero value.
#include <stdlib/EXIT_FAILURE.h>

// EXIT_SUCCESS
	// Successful termination for exit(); evaluates to 0.
#include <stdlib/EXIT_SUCCESS.h>

// {RAND_MAX}
	// Maximum value returned by rand(); at least 32767.
#include <stdlib/RAND_MAX.h>

// The <stdlib.h> header shall define the following macro which shall expand to
// a positive integer expression with type size_t:

// {MB_CUR_MAX}
	// Maximum number of bytes in a character specified by the current locale
	// (category LC_CTYPE).
#include <stdlib/MB_CUR_MAX.h>

// The <stdlib.h> header shall define NULL as described in <stddef.h>.
#include <stddef/NULL.h>

// The <stdlib.h> header shall define the following data types through typedef:

// div_t
	// Structure type returned by the div() function.
#include <stdlib/div_t.h>

// ldiv_t
	// Structure type returned by the ldiv() function.
#include <stdlib/ldiv_t.h>

// lldiv_t
	// Structure type returned by the lldiv() function.
#include <stdlib/lldiv_t.h>

// size_t
	// As described in <stddef.h>.
#include <stddef/size_t.h>

// wchar_t
	// As described in <stddef.h>.
#include <stddef/wchar_t.h>

// In addition, the <stdlib.h> header shall define the following symbolic
// constants and macros as described in <sys/wait.h>:

#include <sys/wait/WEXITSTATUS.h>
#include <sys/wait/WIFEXITED.h>
#include <sys/wait/WIFSIGNALED.h>
#include <sys/wait/WIFSTOPPED.h>
#include <sys/wait/WNOHANG.h>
#include <sys/wait/WSTOPSIG.h>
#include <sys/wait/WTERMSIG.h>
#include <sys/wait/WUNTRACED.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <stdlib/_Exit.h>
#include <stdlib/a64l.h>
#include <stdlib/abort.h>
#include <stdlib/abs.h>
#include <stdlib/atexit.h>
#include <stdlib/atof.h>
#include <stdlib/atoi.h>
#include <stdlib/atol.h>
#include <stdlib/atoll.h>
#include <stdlib/bsearch.h>
#include <stdlib/calloc.h>
#include <stdlib/div.h>
#include <stdlib/drand48.h>
#include <stdlib/erand48.h>
#include <stdlib/exit.h>
#include <stdlib/free.h>
#include <stdlib/getenv.h>
#include <stdlib/getsubopt.h>
#include <stdlib/grantpt.h>
#include <stdlib/initstate.h>
#include <stdlib/jrand48.h>
#include <stdlib/l64a.h>
#include <stdlib/labs.h>
#include <stdlib/lcong48.h>
#include <stdlib/ldiv.h>
#include <stdlib/llabs.h>
#include <stdlib/lldiv.h>
#include <stdlib/lrand48.h>
#include <stdlib/malloc.h>
#include <stdlib/mblen.h>
#include <stdlib/mbstowcs.h>
#include <stdlib/mbtowc.h>
#include <stdlib/mkdtemp.h>
#include <stdlib/mkstemp.h>
#include <stdlib/mrand48.h>
#include <stdlib/nrand48.h>
#include <stdlib/posix_memalign.h>
#include <stdlib/posix_openpt.h>
#include <stdlib/ptsname.h>
#include <stdlib/putenv.h>
#include <stdlib/qsort.h>
#include <stdlib/rand.h>
#include <stdlib/rand_r.h>
#include <stdlib/random.h>
#include <stdlib/realloc.h>
#include <stdlib/realpath.h>
#include <stdlib/seed48.h>
#include <stdlib/setenv.h>
#include <stdlib/setkey.h>
#include <stdlib/setstate.h>
#include <stdlib/srand.h>
#include <stdlib/srand48.h>
#include <stdlib/srandom.h>
#include <stdlib/strtod.h>
#include <stdlib/strtof.h>
#include <stdlib/strtol.h>
#include <stdlib/strtold.h>
#include <stdlib/strtoll.h>
#include <stdlib/strtoul.h>
#include <stdlib/strtoull.h>
#include <stdlib/system.h>
#include <stdlib/unlockpt.h>
#include <stdlib/unsetenv.h>
#include <stdlib/wcstombs.h>
#include <stdlib/wctomb.h>

// Inclusion of the <stdlib.h> header may also make visible all symbols from
// <stddef.h>, <limits.h>, <math.h>, and <sys/wait.h>.

// #include <stddef.h>
// #include <limits.h>
// #include <math.h>
// #include <sys/wait.h>



























#endif
