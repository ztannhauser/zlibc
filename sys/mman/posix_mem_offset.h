
int    posix_mem_offset(const void *restrict, size_t, off_t *restrict,
          size_t *restrict, int *restrict);
