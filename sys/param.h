
#ifndef SYS_PARAM_H
#define SYS_PARAM_H

#include <sys/param/howmany.h>
#include <sys/param/MAX.h>
#include <sys/param/MIN.h>
#include <sys/param/powerof2.h>
#include <sys/param/roundup.h>

// doesn't say in the standard, but glibc also defines:

#include <sys/param/MAXPATHLEN.h>

#endif
