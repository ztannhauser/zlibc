
#ifndef SYS_MMAN_H
#define SYS_MMAN_H

// The <sys/mman.h> header shall define the following symbolic constants for
// use as protection options:

// PROT_EXEC
	// Page can be executed.
#include <sys/mman/PROT_EXEC.h>

// PROT_NONE
	// Page cannot be accessed.
#include <sys/mman/PROT_NONE.h>

// PROT_READ
	// Page can be read.
#include <sys/mman/PROT_READ.h>

// PROT_WRITE
	// Page can be written.
#include <sys/mman/PROT_WRITE.h>

// The <sys/mman.h> header shall define the following symbolic constants for
// use as flag options:

// MAP_FIXED
	// Interpret addr exactly.
#include <sys/mman/MAP_FIXED.h>

// MAP_PRIVATE
	// Changes are private.
#include <sys/mman/MAP_PRIVATE.h>

// MAP_SHARED
	// Share changes.
#include <sys/mman/MAP_SHARED.h>

// Linux Extensions:
#include <sys/mman/MAP_32BIT.h>
#include <sys/mman/MAP_STACK.h>
#include <sys/mman/MAP_GROWSDOWN.h>
#include <sys/mman/MAP_ANONYMOUS.h>

// The <sys/mman.h> header shall define the following symbolic constants for
// the msync() function:

// MS_ASYNC
	// Perform asynchronous writes.
#include <sys/mman/MS_ASYNC.h>

// MS_INVALIDATE
	// Invalidate mappings.
#include <sys/mman/MS_INVALIDATE.h>

// MS_SYNC
	// Perform synchronous writes.
#include <sys/mman/MS_SYNC.h>

// The <sys/mman.h> header shall define the following symbolic constants for
// the mlockall() function:

// MCL_CURRENT
	// Lock currently mapped pages.
#include <sys/mman/MCL_CURRENT.h>

// MCL_FUTURE
	// Lock pages that become mapped.
#include <sys/mman/MCL_FUTURE.h>

// The <sys/mman.h> header shall define the symbolic constant MAP_FAILED which
// shall have type void * and shall be used to indicate a failure from the
// mmap() function.

// If the Advisory Information option is supported, the <sys/mman.h> header
// shall define symbolic constants for the advice argument to the
// posix_madvise() function as follows:

// POSIX_MADV_DONTNEED
	// The application expects that it will not access the specified range in
	// the near future.
#include <sys/mman/POSIX_MADV_DONTNEED.h>

// POSIX_MADV_NORMAL
	// The application has no advice to give on its behavior with respect to
	// the specified range. It is the default characteristic if no advice is
	// given for a range of memory.
#include <sys/mman/POSIX_MADV_NORMAL.h>

// POSIX_MADV_RANDOM
	// The application expects to access the specified range in a random order.
#include <sys/mman/POSIX_MADV_RANDOM.h>

// POSIX_MADV_SEQUENTIAL
	// The application expects to access the specified range sequentially from
	// lower addresses to higher addresses.
#include <sys/mman/POSIX_MADV_SEQUENTIAL.h>

// POSIX_MADV_WILLNEED
	// The application expects to access the specified range in the near future.
#include <sys/mman/POSIX_MADV_WILLNEED.h>

// The <sys/mman.h> header shall define the following symbolic constants for
// use as flags for the posix_typed_mem_open() function:

// POSIX_TYPED_MEM_ALLOCATE
	// Allocate on mmap().
#include <sys/mman/POSIX_TYPED_MEM_ALLOCATE.h>

// POSIX_TYPED_MEM_ALLOCATE_CONTIG
	// Allocate contiguously on mmap().
#include <sys/mman/POSIX_TYPED_MEM_ALLOCATE_CONTIG.h>

// POSIX_TYPED_MEM_MAP_ALLOCATABLE
	// Map on mmap(), without affecting allocatability.
#include <sys/mman/POSIX_TYPED_MEM_MAP_ALLOCATABLE.h>

// The <sys/mman.h> header shall define the mode_t, off_t, and size_t types as
// described in <sys_types.h>.
#include <sys/types/mode_t.h>
#include <sys/types/off_t.h>
#include <sys/types/size_t.h>

// The <sys/mman.h> header shall define the posix_typed_mem_info structure,
// which shall include at least the following member:

// size_t  posix_tmi_length;
	// Maximum length which may be allocatedfrom a typed memory object.

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <sys/mman/mlock.h>
#include <sys/mman/mlockall.h>
#include <sys/mman/mmap.h>
#include <sys/mman/mprotect.h>
#include <sys/mman/msync.h>
#include <sys/mman/munlock.h>
#include <sys/mman/munlockall.h>
#include <sys/mman/munmap.h>
#include <sys/mman/posix_madvise.h>
#include <sys/mman/posix_mem_offset.h>
#include <sys/mman/posix_typed_mem_get_info.h>
#include <sys/mman/posix_typed_mem_open.h>
#include <sys/mman/shm_open.h>
#include <sys/mman/shm_unlink.h>

#endif

















