
#ifndef STRUCT_STAT_H
#define STRUCT_STAT_H

// The <sys/stat.h> header shall define the structure of the data returned by
// the fstat(), lstat(), and stat() functions.

// The <sys/stat.h> header shall define the stat structure, which shall include
// at least the following members:

#include <sys/types/dev_t.h>
#include <sys/types/ino_t.h>
#include <sys/types/nlink_t.h>
#include <sys/types/mode_t.h>
#include <sys/types/uid_t.h>
#include <sys/types/gid_t.h>
#include <sys/types/off_t.h>
#include <sys/types/blksize_t.h>
#include <sys/types/blkcnt_t.h>

#include <time/timespec.h>

struct stat
{
	// dev_t st_dev;            // Device ID of device containing file.
	// ino_t st_ino;            // File serial number.
	// mode_t st_mode;          // Mode of file (see below).
	// nlink_t st_nlink;        // Number of hard links to the file.
	// uid_t st_uid;            // User ID of file.
	// gid_t st_gid;            // Group ID of file.
	// dev_t st_rdev;           // Device ID (if file is character or block special).
	// off_t st_size;           // For regular files, the file size in bytes.
	//                          // For symbolic links, the length in bytes of the
	//                          // pathname contained in the symbolic link.
	//                          // For a shared memory object, the length in bytes.
	//                          // For a typed memory object, the length in bytes.
	//                          // For other file types, the use of this field is
	//                          // unspecified.
	// struct timespec st_atim; // Last data access timestamp.
	// struct timespec st_mtim; // Last data modification timestamp.
	// struct timespec st_ctim; // Last file status change timestamp.
	// blksize_t st_blksize;    // A file system-specific preferred I/O block size
	//                          // for this object. In some file system types, this
	//                          // may vary from file to file.
	// blkcnt_t st_blocks;      // Number of blocks allocated for this object.

	dev_t st_dev;
	ino_t st_ino;

	nlink_t st_nlink;
	mode_t st_mode;

	uid_t st_uid;
	gid_t st_gid;

	int __pad0;

	dev_t st_rdev;

	off_t st_size;
	blksize_t st_blksize;

	blkcnt_t st_blocks;
	struct timespec st_atim;
	struct timespec st_mtim;
	struct timespec st_ctim;
	
	#define st_atime st_atim.tv_sec
	#define st_mtime st_mtim.tv_sec
	#define st_ctime st_ctim.tv_sec

	signed long __glibc_reserved[3];
};

// The st_ino and st_dev fields taken together uniquely identify the file
// within the system.

int stat(const char *pathname, struct stat *statbuf);

#endif












