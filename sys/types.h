
#ifndef SYS_TYPES_H
#define SYS_TYPES_H

// The <sys/types.h> header shall define at least the following types:

// blkcnt_t
	// Used for file block counts.
#include <sys/types/blkcnt_t.h>

// blksize_t
	// Used for block sizes.
#include <sys/types/blksize_t.h>

// clock_t
	// Used for system times in clock ticks or CLOCKS_PER_SEC; see <time.h>.
#include <sys/types/clock_t.h>

// clockid_t
	// Used for clock ID type in the clock and timer functions.
#include <sys/types/clockid_t.h>

// dev_t
	// Used for device IDs.
#include <sys/types/dev_t.h>

// fsblkcnt_t
	// Used for file system block counts.
#include <sys/types/fsblkcnt_t.h>

// fsfilcnt_t
	// Used for file system file counts.
#include <sys/types/fsfilcnt_t.h>

// gid_t
	// Used for group IDs.
#include <sys/types/gid_t.h>

// id_t
	// Used as a general identifier; can be used to contain at least a pid_t,
	// uid_t, or gid_t.
#include <sys/types/id_t.h>

// ino_t
	// Used for file serial numbers.
#include <sys/types/ino_t.h>

// key_t
	// Used for XSI interprocess communication.
#include <sys/types/key_t.h>

// mode_t
	// Used for some file attributes.
#include <sys/types/mode_t.h>

// nlink_t
	// Used for link counts.
#include <sys/types/nlink_t.h>

// off_t
	// Used for file sizes.
#include <sys/types/off_t.h>

// pid_t
	// Used for process IDs and process group IDs.
#include <sys/types/pid_t.h>

// pthread_attr_t
	// Used to identify a thread attribute object.
#include <sys/types/pthread_attr_t.h>

// pthread_barrier_t
	// Used to identify a barrier.
#include <sys/types/pthread_barrier_t.h>

// pthread_barrierattr_t
	// Used to define a barrier attributes object.
#include <sys/types/pthread_barrierattr_t.h>

// pthread_cond_t
	// Used for condition variables.
#include <sys/types/pthread_cond_t.h>

// pthread_condattr_t
	// Used to identify a condition attribute object.
#include <sys/types/pthread_condattr_t.h>

// pthread_key_t
	// Used for thread-specific data keys.
#include <sys/types/pthread_key_t.h>

// pthread_mutex_t
	// Used for mutexes.
#include <sys/types/pthread_mutex_t.h>

// pthread_mutexattr_t
	// Used to identify a mutex attribute object.
#include <sys/types/pthread_mutexattr_t.h>

// pthread_once_t
	// Used for dynamic package initialization.
#include <sys/types/pthread_once_t.h>

// pthread_rwlock_t
	// Used for read-write locks.
#include <sys/types/pthread_rwlock_t.h>

// pthread_rwlockattr_t
	// Used for read-write lock attributes.
#include <sys/types/pthread_rwlockattr_t.h>

// pthread_spinlock_t
	// Used to identify a spin lock.
#include <sys/types/pthread_spinlock_t.h>

// pthread_t
	// Used to identify a thread.
#include <sys/types/pthread_t.h>

// size_t
	// Used for sizes of objects.
#include <sys/types/size_t.h>

// ssize_t
	// Used for a count of bytes or an error indication.
#include <sys/types/ssize_t.h>

// suseconds_t
	// Used for time in microseconds.
#include <sys/types/suseconds_t.h>

// time_t
	// Used for time in seconds.
#include <sys/types/time_t.h>

// timer_t
	// Used for timer ID returned by timer_create().
#include <sys/types/timer_t.h>

// trace_attr_t
	// Used to identify a trace stream attributes object
#include <sys/types/trace_attr_t.h>

// trace_event_id_t
	// Used to identify a trace event type.
#include <sys/types/trace_event_id_t.h>

// trace_event_set_t
	// Used to identify a trace event type set.
#include <sys/types/trace_event_set_t.h>

// trace_id_t
	// Used to identify a trace stream.
#include <sys/types/trace_id_t.h>

// uid_t
	// Used for user IDs.
#include <sys/types/uid_t.h>

// All of the types shall be defined as arithmetic types of an appropriate
// length, with the following exceptions:

// pthread_attr_t
// pthread_barrier_t
// pthread_barrierattr_t
// pthread_cond_t
// pthread_condattr_t
// pthread_key_t
// pthread_mutex_t
// pthread_mutexattr_t
// pthread_once_t
// pthread_rwlock_t
// pthread_rwlockattr_t
// pthread_spinlock_t
// pthread_t
// trace_attr_t
// trace_event_id_t
// trace_event_set_t
// trace_id_t

// Additionally:

// *  mode_t shall be an integer type.

// *  dev_t shall be an integer type.

// *  nlink_t, uid_t, gid_t, and id_t shall be integer types.

// *  blkcnt_t and off_t shall be signed integer types.

// *  fsblkcnt_t, fsfilcnt_t, and ino_t shall be defined as unsigned integer types.

// *  size_t shall be an unsigned integer type.

// *  blksize_t, pid_t, and ssize_t shall be signed integer types.

// *  clock_t shall be an integer or real-floating type.  time_t shall be an integer type.

// The type ssize_t shall be capable of storing values at least in the
// range [−1, {SSIZE_MAX}].

// The type suseconds_t shall be a signed integer type capable of storing
// values at least in the range [−1, 1000000].

// The implementation shall support one or more programming environments in
// which the widths of blksize_t, pid_t, size_t, ssize_t, and suseconds_t are
// no greater than the width of type long. The names of these programming
// environments can be obtained using the confstr() function or the getconf
// utility.

// There are no defined comparison or assignment operators for the following types:

// pthread_attr_t
// pthread_barrier_t
// pthread_barrierattr_t
// pthread_cond_t
// pthread_condattr_t
// pthread_mutex_t
// pthread_mutexattr_t
// pthread_rwlock_t
// pthread_rwlockattr_t
// pthread_spinlock_t
// trace_attr_t

#endif























