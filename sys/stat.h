
#ifndef SYS_STAT_H
#define SYS_STAT_H

// The <sys/stat.h> header shall define the structure of the data returned by
// the fstat(), lstat(), and stat() functions.

// The <sys/stat.h> header shall define the stat structure:
#include <sys/stat/stat.h>

// The <sys/stat.h> header shall define the blkcnt_t, blksize_t, dev_t, ino_t,
// mode_t, nlink_t, uid_t, gid_t, off_t, and time_t types as described in
// <sys/types.h>.
#include <sys/types/blkcnt_t.h>
#include <sys/types/blksize_t.h>
#include <sys/types/dev_t.h>
#include <sys/types/ino_t.h>
#include <sys/types/nlink_t.h>
#include <sys/types/uid_t.h>
#include <sys/types/gid_t.h>
#include <sys/types/off_t.h>
#include <sys/types/time_t.h>

// The <sys/stat.h> header shall define the timespec structure as described in
// <time.h>. Times shall be given in seconds since the Epoch.
#include <time/timespec.h>

// Which structure members have meaningful values depends on the type of file.
// For further information, see the descriptions of fstat(), lstat(), and
// stat() in the System Interfaces volume of POSIX.1‐2008.

// For compatibility with earlier versions of this standard, the st_atime macro
// shall be defined with the value st_atim.tv_sec.  Similarly, st_ctime and
// st_mtime shall be defined as macros with the values st_ctim.tv_sec and
// st_mtim.tv_sec, respectively.

// The <sys/stat.h> header shall define the following symbolic constants for
// the file types encoded in type mode_t.  The values shall be suitable for use
// in #if preprocessing directives:

// S_IFMT
	// Type of file.
#include <sys/stat/S_IFMT.h>

// S_IFBLK
	// Block special.
#include <sys/stat/S_IFBLK.h>

// S_IFCHR
	// Character special.
#include <sys/stat/S_IFCHR.h>

// S_IFIFO
	// FIFO special.
#include <sys/stat/S_IFIFO.h>

// S_IFREG
	// Regular.
#include <sys/stat/S_IFREG.h>

// S_IFDIR
	// Directory.
#include <sys/stat/S_IFDIR.h>

// S_IFLNK
	// Symbolic link.
#include <sys/stat/S_IFLNK.h>

// S_IFSOCK
	// Socket.
#include <sys/stat/S_IFSOCK.h>

// The  <sys/stat.h>  header  shall  define  the following symbolic constants
// for the file mode bits encoded in type mode_t, with the indicated numeric
// values. These macros shall expand to an expression which has a type that
// allows them to be used, either singly or OR'ed together, as the third
// argument to  open() without the need for a mode_t cast. The values shall be
// suitable for use in #if preprocessing directives.

// ┌─────────┬───────────────┬──────────────────────────────────────────────┐
// │  Name   │ Numeric Value │                 Description                  │
// ├─────────┼───────────────┼──────────────────────────────────────────────┤
// │ S_IRWXU │      0700     │ Read, write, execute/search by owner.        │
// │ S_IRUSR │      0400     │ Read permission, owner.                      │
// │ S_IWUSR │      0200     │ Write permission, owner.                     │
// │ S_IXUSR │      0100     │ Execute/search permission, owner.            │
// ├─────────┼───────────────┼──────────────────────────────────────────────┤
// │ S_IRWXG │       070     │ Read, write, execute/search by group.        │
// │ S_IRGRP │       040     │ Read permission, group.                      │
// │ S_IWGRP │       020     │ Write permission, group.                     │
// │ S_IXGRP │       010     │ Execute/search permission, group.            │
// ├─────────┼───────────────┼──────────────────────────────────────────────┤
// │ S_IRWXO │        07     │ Read, write, execute/search by others.       │
// │ S_IROTH │        04     │ Read permission, others.                     │
// │ S_IWOTH │        02     │ Write permission, others.                    │
// │ S_IXOTH │        01     │ Execute/search permission, others.           │
// ├─────────┼───────────────┼──────────────────────────────────────────────┤
// │ S_ISUID │     04000     │ Set-user-ID on execution.                    │
// │ S_ISGID │     02000     │ Set-group-ID on execution.                   │
// │ S_ISVTX │     01000     │ On directories, restricted deletion flag.    │
// └─────────┴───────────────┴──────────────────────────────────────────────┘

#include <sys/stat/S_IRWXU.h>
#include <sys/stat/S_IRUSR.h>
#include <sys/stat/S_IWUSR.h>
#include <sys/stat/S_IXUSR.h>

#include <sys/stat/S_IRWXG.h>
#include <sys/stat/S_IRGRP.h>
#include <sys/stat/S_IWGRP.h>
#include <sys/stat/S_IXGRP.h>

#include <sys/stat/S_IRWXO.h>
#include <sys/stat/S_IROTH.h>
#include <sys/stat/S_IWOTH.h>
#include <sys/stat/S_IXOTH.h>

#include <sys/stat/S_ISUID.h>
#include <sys/stat/S_ISGID.h>
#include <sys/stat/S_ISVTX.h>

// The  following  macros  shall  be provided to test whether a file is of the
// specified type. The value m supplied to the macros is the value of st_mode
// from a stat structure. The macro shall evaluate to a non-zero value if the
// test is true; 0 if the test is false.

// S_ISBLK(m)
	// Test for a block special file.
#include <sys/stat/S_ISBLK.h>

// S_ISCHR(m)
	// Test for a character special file.
#include <sys/stat/S_ISCHR.h>

// S_ISDIR(m)
	// Test for a directory.
#include <sys/stat/S_ISDIR.h>

// S_ISFIFO(m)
	// Test for a pipe or FIFO special file.
#include <sys/stat/S_ISFIFO.h>

// S_ISREG(m)
	// Test for a regular file.
#include <sys/stat/S_ISREG.h>

// S_ISLNK(m)
	// Test for a symbolic link.
#include <sys/stat/S_ISLNK.h>

// S_ISSOCK(m)
	// Test for a socket.
#include <sys/stat/S_ISSOCK.h>

// The implementation may implement message queues, semaphores, or shared
// memory objects as distinct file types. The following  macros  shall  be
// provided to test whether a file is of the specified type. The value of the
// buf argument supplied to the macros is a pointer to a stat structure. The
// macro shall evaluate to a nonzero value if the specified object is
// implemented as a distinct file type and the specified file type is contained
// in the stat structure referenced by buf. Otherwise, the macro shall evaluate
// to zero.

// S_TYPEISMQ(buf)
	// Test for a message queue.
#include <sys/stat/S_TYPEISMQ.h>

// S_TYPEISSEM(buf)
	// Test for a semaphore.
#include <sys/stat/S_TYPEISSEM.h>

// S_TYPEISSHM(buf)
	// Test for a shared memory object.
#include <sys/stat/S_TYPEISSHM.h>

// The implementation may implement typed memory objects as distinct file
// types, and the following macro shall test whether a file is of the specified
// type. The value of the buf argument supplied to the macros is a pointer to a
// stat structure. The macro shall evaluate to a non-zero value if the
// specified object is implemented as a distinct file type and the specified
// file type is contained in the stat structure referenced by buf. Otherwise,
// the macro shall evaluate to zero.

// S_TYPEISTMO(buf)
	// Test macro for a typed memory object.
#include <sys/stat/S_TYPEISTMO.h>

// The  <sys/stat.h>  header shall define the following symbolic constants as
// distinct integer values outside of the range [0,999999999], for use with the
// futimens() and utimensat() functions: UTIME_NOW UTIME_OMIT
#include <sys/stat/UTIME_NOW.h>
#include <sys/stat/UTIME_OMIT.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <sys/stat/chmod.h>
#include <sys/stat/fchmod.h>
#include <sys/stat/fchmodat.h>
#include <sys/stat/fstat.h>
#include <sys/stat/fstatat.h>
#include <sys/stat/futimens.h>
#include <sys/stat/lstat.h>
#include <sys/stat/mkdir.h>
#include <sys/stat/mkdirat.h>
#include <sys/stat/mkfifo.h>
#include <sys/stat/mkfifoat.h>
#include <sys/stat/mknod.h>
#include <sys/stat/mknodat.h>
#include <sys/stat/stat.h>
#include <sys/stat/umask.h>
#include <sys/stat/utimensat.h>

#endif
