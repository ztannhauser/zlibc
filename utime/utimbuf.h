
// The <utime.h> header shall declare the utimbuf structure, which shall
// include the following members:

struct utimbuf
{
	time_t actime;  // Access time.
	time_t modtime; // Modification time.
};


