

#ifdef DEBUG_BUILD
#include <debug.h>
#endif

#include <sys/syscall/SYS_read.h>
#include <sys/syscall/syscall.h>

#include "read.h"

ssize_t read(int fd, void * ptr, size_t len)
{
	ssize_t retval = syscall(SYS_read, fd, ptr, len);
	
	#ifdef DEBUG_BUILD
	ddprintf("\e[37m" "read(fd = %i, ptr = %p, len = %lu) -> %lu" "\e[m" "\n",
		fd, ptr, len, retval);
	#endif
	
	return retval;
}
