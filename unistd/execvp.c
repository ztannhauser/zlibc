
#include "globals.h"
#include "execvpe.h"
#include "execvp.h"

int execvp(const char *file, char *const argv[])
{
	return execvpe(file, argv, environ);
}
