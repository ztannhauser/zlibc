
#ifdef DEBUG_BUILD
#include <debug.h>
#endif

#include <sys/types/mode_t.h>

#include <fcntl/O_CREAT.h>

#include <stdarg/va_list.h>
#include <stdarg/va_start.h>
#include <stdarg/va_arg.h>
#include <stdarg/va_end.h>

#include <sys/syscall/SYS_open.h>
#include <sys/syscall/syscall.h>

#include "open.h"

int open(const char * pathname, int flags, ...)
{
	mode_t mode;
	
	if (flags & O_CREAT)
	{
		va_list va;
		va_start(va, flags);
		mode = va_arg(va, mode_t);
		va_end(va);
	}
	
	int fd = syscall(SYS_open, pathname, flags, mode);
	
	#ifdef DEBUG_BUILD
	if (flags & O_CREAT)
	{
		ddprintf(
			"\e[37m" "open(pathname = \"%s\", flags = %x, mode = %o) -> %i"
			"\e[m" "\n",
			pathname, flags, mode, fd);
	}
	else
	{
		ddprintf(
			"\e[37m" "open(pathname = \"%s\", flags = %x) -> %i"
			"\e[m" "\n",
			pathname, flags, fd);
	}
	#endif
	
	return fd;
}

























