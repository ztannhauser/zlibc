
#include "globals.h"
#include "execve.h"
#include "execv.h"

int execv(const char *pathname, char *const argv[])
{
	return execve(pathname, argv, environ);
}

