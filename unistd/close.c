
#ifdef DEBUG_BUILD
#include <debug.h>
#endif

#include <sys/syscall/SYS_close.h>
#include <sys/syscall/syscall.h>

#include "close.h"

int close(int fd)
{
	int retval = syscall(SYS_close, fd);
	
	#ifdef DEBUG_BUILD
	ddprintf("\e[37m" "close(fd = %i) -> %i;" "\e[m" "\n", fd, retval);
	#endif
	
	return retval;
}
