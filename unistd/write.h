
#include <sys/types/size_t.h>
#include <sys/types/ssize_t.h>

ssize_t      write(int, const void *, size_t);
