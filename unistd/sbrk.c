
#include <stddef/NULL.h>

#include "memory_globals.h"

#include "_brk.h"
#include "sbrk.h"

void *sbrk(intptr_t increment)
{
	void *oldend, *newend;
	
	if (!program_break_end)
		program_break_start =
		program_break_end   = _brk(NULL);
	
	if (oldend = program_break_end,
		_brk(newend = program_break_end + increment) < 0)
		return (void*) -1;
	else
		return program_break_end = newend, oldend;
}













