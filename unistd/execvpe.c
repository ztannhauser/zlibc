
#include <limits/PATH_MAX.h>

#include <strings/index.h>

#include <string/stpcpy.h>
#include <string/stpncpy.h>

#include <stdlib/getenv.h>

#include <errno/errno.h>
#include <errno/EACCES.h>

#include "execve.h"
#include "execvpe.h"

int execvpe(const char *file, char *const argv[], char *const envp[])
{
	int retval;
	char abspath[PATH_MAX];
	
	if (index(file, '/'))
		retval = execve(file, argv, envp);
	else for (char *path = getenv("PATH") ?: "/bin:/usr/bin",
		*start = path, *end; *start; start = end + !!*end)
	{
		for (end = start + 1; *end && *end != ':'; end++);
		
		stpcpy(stpcpy(stpncpy(abspath, start, end - start), "/"), file);
		
		retval = execve(abspath, argv, envp);
	}
	
	return retval;
}














