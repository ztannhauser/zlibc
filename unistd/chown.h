
#include <sys/types/uid_t.h>
#include <sys/types/gid_t.h>

int          chown(const char *, uid_t, gid_t);
