
#include <assert/assert.h>

#include <sys/ioctl/ioctl.h>
#include <sys/ioctl/TIOCGWINSZ.h>

#include <termios/winsize.h>

#include "isatty.h"

int isatty(int fd)
{
	struct winsize arg;
	return !ioctl(fd, TIOCGWINSZ, &arg);
}

