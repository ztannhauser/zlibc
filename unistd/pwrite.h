
#include <sys/types/ssize_t.h>
#include <sys/types/off_t.h>

#include <stddef/size_t.h>

ssize_t      pwrite(int, const void *, size_t, off_t);

