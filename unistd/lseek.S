

// off_t lseek(int fd, off_t offset, int whence)

.global lseek

lseek:
	endbr64
	pushq %rbp
	movq %rsp, %rbp
	
	movq   $8, %rax // indicate we want to all lseek()
	movl %edi, %edi // pass fd     to syscall
	movq %rsi, %rsi // pass offset to syscall
	movl %edx, %edx // pass whence to syscall
	syscall
	
	cmp $0, %rax
	jle .else // if (%rax >= 0):
		movq %rax, %rax
		jmp .done
	.else:
		neg %rax
		movq %rax, errno(%rip) // set errno = -%rax
		movq $-1, %rax // return -1
	.done:
	leave
	ret























