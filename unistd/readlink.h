
#include <stddef/size_t.h>

#include <sys/types/ssize_t.h>

ssize_t      readlink(const char *restrict, char *restrict, size_t);
