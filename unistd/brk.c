
#include <errno/errno.h>
#include <errno/ENOMEM.h>

#include "_brk.h"
#include "brk.h"

int brk(void *addr)
{
	if (_brk(addr) == addr)
		return errno = ENOMEM, -1;
	else
		return 0;
}
