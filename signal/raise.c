
#include <signal/kill.h>

#include <unistd/getpid.h>

#include "raise.h"

int raise(int sig)
{
	return kill(getpid(), sig);
}


