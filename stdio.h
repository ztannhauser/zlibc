
#ifndef STDIO_H
#define STDIO_H

// Some of the functionality described on this reference page extends the ISO C
// standard. Applications shall define the appropriate feature test  macro
// (see  the  System  Interfaces volume of POSIX.1‐2008, Section 2.2, The
// Compilation Environment) to enable the visibility of these symbols in this
// header.

// The <stdio.h> header shall define the following data types through typedef:

// FILE
	// A structure containing information about a file.
#include <stdio/FILE.h>

// fpos_t
	// A non-array type containing all information needed to specify uniquely
	// every position within a file.
#include <stdio/fpos_t.h>

// off_t
	// As described in <sys/types.h>.
#include <sys/types/off_t.h>

// size_t
	// As described in <stddef.h>.
#include <stddef/size_t.h>

// ssize_t
	// As described in <sys/types.h>.
#include <sys/types/ssize_t.h>

// va_list
	// As described in <stdarg.h>.
#include <stdarg/va_list.h>

// The <stdio.h> header shall define the following macros which shall expand to
// integer constant expressions:

// BUFSIZ
	// Size of <stdio.h> buffers. This shall expand to a positive value.
#include <stdio/BUFSIZ.h>

// L_ctermid
	// Maximum size of character array to hold ctermid() output.
#include <stdio/L_ctermid.h>

// L_tmpnam
	// Maximum size of character array to hold tmpnam() output.
#include <stdio/L_tmpnam.h>

// The <stdio.h> header shall define the following macros which shall expand to
// integer constant expressions with distinct values:

// _IOFBF
	// Input/output fully buffered.
#include <stdio/_IOFBF.h>

// _IOLBF
	// Input/output line buffered.
#include <stdio/_IOLBF.h>

// _IONBF
	// Input/output unbuffered.
#include <stdio/_IONBF.h>

// The <stdio.h> header shall define the following macros which shall expand to
// integer constant expressions with distinct values:

// SEEK_CUR
	// Seek relative to current position.
#include <stdio/SEEK_CUR.h>

// SEEK_END
	// Seek relative to end-of-file.
#include <stdio/SEEK_END.h>

// SEEK_SET
	// Seek relative to start-of-file.
#include <stdio/SEEK_SET.h>

// The <stdio.h> header shall define the following macros which shall expand to
// integer constant expressions denoting implementation limits:

// {FILENAME_MAX}
	// Maximum size in bytes of the longest pathname that the implementation
	// guarantees can be opened.
#include <stdio/FILENAME_MAX.h>

// {FOPEN_MAX}
	// Number of streams which the implementation guarantees can be open
	// simultaneously. The value is at least eight.
#include <stdio/FOPEN_MAX.h>

// {TMP_MAX}
	// Minimum  number of unique filenames generated by tmpnam().  Maximum
	// number of times an application can call tmpnam() reliably. The value of
	// {TMP_MAX} is at least 25.
#include <stdio/TMP_MAX.h>

// On XSI-conformant systems, the value of {TMP_MAX} is at least 10000.

// The <stdio.h> header shall define the following macro which shall expand to
// an integer constant expression with type int and a negative value:

// EOF
	// End-of-file return value.
#include <stdio/EOF.h>

// The <stdio.h> header shall define NULL as described in <stddef.h>.
#include <stddef/NULL.h>

// The <stdio.h> header shall define the following macro which shall expand to
// a string constant:

// P_tmpdir
	// Default directory prefix for tempnam().
#include <stdio/P_tmpdir.h>

// The  <stdio.h>  header shall define the following macros which shall expand
// to expressions of type ``pointer to FILE'' that point to the FILE objects
// associated, respectively, with the standard error, input, and output streams:

// stderr
	// Standard error output stream.
#include <stdio/stderr.h>

// stdin
	// Standard input stream.
#include <stdio/stdin.h>

// stdout
	// Standard output stream.
#include <stdio/stdout.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <stdio/clearerr.h>
#include <stdio/ctermid.h>
#include <stdio/dprintf.h>
#include <stdio/fclose.h>
#include <stdio/fdopen.h>
#include <stdio/feof.h>
#include <stdio/ferror.h>
#include <stdio/fflush.h>
#include <stdio/fgetc.h>
#include <stdio/fgetpos.h>
#include <stdio/fgets.h>
#include <stdio/fileno.h>
#include <stdio/flockfile.h>
#include <stdio/fmemopen.h>
#include <stdio/fopen.h>
#include <stdio/fprintf.h>
#include <stdio/fputc.h>
#include <stdio/fputs.h>
#include <stdio/fread.h>
#include <stdio/freopen.h>
#include <stdio/fscanf.h>
#include <stdio/fseek.h>
#include <stdio/fseeko.h>
#include <stdio/fsetpos.h>
#include <stdio/ftell.h>
#include <stdio/ftello.h>
#include <stdio/ftrylockfile.h>
#include <stdio/funlockfile.h>
#include <stdio/fwrite.h>
#include <stdio/getc.h>
#include <stdio/getchar.h>
#include <stdio/getchar_unlocked.h>
#include <stdio/getc_unlocked.h>
#include <stdio/getdelim.h>
#include <stdio/getline.h>
#include <stdio/gets.h>
#include <stdio/open_memstream.h>
#include <stdio/pclose.h>
#include <stdio/perror.h>
#include <stdio/popen.h>
#include <stdio/printf.h>
#include <stdio/putc.h>
#include <stdio/putchar.h>
#include <stdio/putchar_unlocked.h>
#include <stdio/putc_unlocked.h>
#include <stdio/puts.h>
#include <stdio/remove.h>
#include <stdio/renameat.h>
#include <stdio/rename.h>
#include <stdio/rewind.h>
#include <stdio/scanf.h>
#include <stdio/setbuf.h>
#include <stdio/setvbuf.h>
#include <stdio/snprintf.h>
#include <stdio/sprintf.h>
#include <stdio/sscanf.h>
#include <stdio/tempnam.h>
#include <stdio/tmpfile.h>
#include <stdio/tmpnam.h>
#include <stdio/ungetc.h>
#include <stdio/vdprintf.h>
#include <stdio/vfprintf.h>
#include <stdio/vfscanf.h>
#include <stdio/vprintf.h>
#include <stdio/vscanf.h>
#include <stdio/vsnprintf.h>
#include <stdio/vsprintf.h>
#include <stdio/vsscanf.h>

// Inclusion of the <stdio.h> header may also make visible all symbols from
// <stddef.h>:
// #include <stddef.h>












#endif
