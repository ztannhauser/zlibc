
// The <tar.h> header shall define the following symbolic constants with the indicated values.
// 
// General definitions:
// 
// ┌────────────────┬─────────────────┬──────────────────────────────────────────┐
// │     Name       │      Value      │               Description                │
// ├────────────────┼─────────────────┼──────────────────────────────────────────┤
// │TMAGIC          │     "ustar"     │ ustar plus null byte.                    │
// │TMAGLEN         │        6        │ Length of the above.                     │
// │TVERSION        │      "00"       │ 00 without a null byte.                  │
// │TVERSLEN        │        2        │ Length of the above.                     │
// └────────────────┴─────────────────┴──────────────────────────────────────────┘

#include <tar/TMAGIC.h>
#include <tar/TMAGLEN.h>
#include <tar/TVERSION.h>
#include <tar/TVERSLEN.h>

// Typeflag field definitions:
// 
// ┌────────────────┬─────────────────┬──────────────────────────────────────────┐
// │     Name       │      Value      │               Description                │
// ├────────────────┼─────────────────┼──────────────────────────────────────────┤
// │REGTYPE         │       '0'       │ Regular file.                            │
// │AREGTYPE        │      '\0'       │ Regular file.                            │
// │LNKTYPE         │       '1'       │ Link.                                    │
// │SYMTYPE         │       '2'       │ Symbolic link.                           │
// │CHRTYPE         │       '3'       │ Character special.                       │
// │BLKTYPE         │       '4'       │ Block special.                           │
// │DIRTYPE         │       '5'       │ Directory.                               │
// │FIFOTYPE        │       '6'       │ FIFO special.                            │
// │CONTTYPE        │       '7'       │ Reserved.                                │
// └────────────────┴─────────────────┴──────────────────────────────────────────┘

#include <tar/REGTYPE.h>
#include <tar/AREGTYPE.h>
#include <tar/LNKTYPE.h>
#include <tar/SYMTYPE.h>
#include <tar/CHRTYPE.h>
#include <tar/BLKTYPE.h>
#include <tar/DIRTYPE.h>
#include <tar/FIFOTYPE.h>
#include <tar/CONTTYPE.h>

// Mode field bit definitions (octal):
// 
// ┌────────────────┬─────────────────┬──────────────────────────────────────────────┐
// │     Name       │      Value      │                 Description                  │
// ├────────────────┼─────────────────┼──────────────────────────────────────────────┤
// │TSUID           │      04000      │ Set UID on execution.                        │
// │TSGID           │      02000      │ Set GID on execution.                        │
// │TSVTX           │      01000      │ On directories, restricted deletion flag.    │
// │TUREAD          │      00400      │ Read by owner.                               │
// │TUWRITE         │      00200      │ Write by owner special.                      │
// │TUEXEC          │      00100      │ Execute/search by owner.                     │
// │TGREAD          │      00040      │ Read by group.                               │
// │TGWRITE         │      00020      │ Write by group.                              │
// │TGEXEC          │      00010      │ Execute/search by group.                     │
// │TOREAD          │      00004      │ Read by other.                               │
// │TOWRITE         │      00002      │ Write by other.                              │
// │TOEXEC          │      00001      │ Execute/search by other.                     │
// └────────────────┴─────────────────┴──────────────────────────────────────────────┘

#include <tar/TSUID.h>
#include <tar/TSGID.h>
#include <tar/TSVTX.h>
#include <tar/TUREAD.h>
#include <tar/TUWRITE.h>
#include <tar/TUEXEC.h>
#include <tar/TGREAD.h>
#include <tar/TGWRITE.h>
#include <tar/TGEXEC.h>
#include <tar/TOREAD.h>
#include <tar/TOWRITE.h>
#include <tar/TOEXEC.h>




















