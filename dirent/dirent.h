
#include <sys/types/ino_t.h>

#include <limits/NAME_MAX.h>

// It shall also define the structure dirent which shall include the following
// members:

struct dirent
{
	ino_t  d_ino;              // File serial number.
	char   d_name[NAME_MAX+1]; // Filename string of entry.
};

// The array d_name is of unspecified size, but shall contain a filename of at
// most {NAME_MAX} bytes followed by a terminating null byte.

