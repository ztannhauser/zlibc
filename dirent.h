
// The internal format of directories is unspecified.

// The <dirent.h> header shall define the following type:

// DIR
	// A type representing a directory stream. The DIR type may be an
	// incomplete type.
struct DIR;
typedef struct DIR DIR;

// It shall also define the structure dirent:
#include <dirent/dirent.h>

// The <dirent.h> header shall define the ino_t type as described in
// <sys/types.h>.
#include <sys/types/ino_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <dirent/alphasort.h>
#include <dirent/closedir.h>
#include <dirent/dirfd.h>
#include <dirent/fdopendir.h>
#include <dirent/opendir.h>
#include <dirent/readdir.h>
#include <dirent/readdir_r.h>
#include <dirent/rewinddir.h>
#include <dirent/scandir.h>
#include <dirent/seekdir.h>
#include <dirent/telldir.h>


