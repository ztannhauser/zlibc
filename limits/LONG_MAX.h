
#if (defined __x86_64__)

#define LONG_MAX 0x7fffffffffffffffL

#else

#define LONG_MAX 0x7fffffffL

#endif
