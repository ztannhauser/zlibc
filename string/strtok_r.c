
#include <stddef/NULL.h>

#include <strings/index.h>

#include "strspn.h"
#include "strcspn.h"
#include "strtok_r.h"

char *strtok_r(
	char *restrict string,
	const char *restrict delim,
	char **restrict saveptr)
{
	char* retval;
	
	if (string)
		*saveptr = string;
	
	*saveptr += strspn(*saveptr, delim);
	
	if (!*(retval = *saveptr))
		retval = NULL;
	else if (*(*saveptr += strcspn(*saveptr, delim)))
		*(*saveptr)++ = '\0';
	
	return retval;
}


































