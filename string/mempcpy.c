
#include "mempcpy.h"

void *mempcpy(void *_dest, const void *_src, size_t n)
{
	unsigned char* dest = _dest;
	const unsigned char* src = _src;
	
	while (n--) *dest++ = *src++;
	
	return dest;
}
