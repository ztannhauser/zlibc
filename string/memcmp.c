
#include "memcmp.h"

int memcmp(const void *s1, const void *s2, size_t n)
{
	int diff;
	const unsigned char *u1 = s1, *u2 = s2;
	
	while (n-- && !(diff = *u1++ - *u2++));
	
	return diff;
}

