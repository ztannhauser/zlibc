
#include "strncmp.h"

int strncmp(
	const char *s1,
	const char *s2, size_t n)
{
	int cmp = 0;
	
	while (n-- && !(cmp = *s1++ - *s2++) && s1[-1]);
	
	return cmp;
}

