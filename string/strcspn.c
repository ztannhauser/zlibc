
#include <strings/index.h>

#include "strcspn.h"

size_t strcspn(const char *s, const char *reject)
{
	const char* const t = s;
	
	while (!index(reject, *s)) s++;
	
	return s - t;
}
