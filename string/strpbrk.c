
#include <strings/index.h>

#include <stddef/NULL.h>

#include "strpbrk.h"

char* strpbrk(const char * s, const char * accept)
{
	while (*s && !index(accept, *s)) s++;
	
	return *s ? (char*) s : NULL;
}
