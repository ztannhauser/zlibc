
/*#include <assert/assert.h>*/
/*#include <stdio/stdout.h>*/
/*#include <stdio/fflush.h>*/

#include "stpcpy.h"

char *stpcpy(char *dest, const char *src)
{
	while ((*dest++ = *src++));
	return dest - 1;
}

