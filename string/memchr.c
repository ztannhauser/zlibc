
#include <stddef/NULL.h>

#include "memchr.h"

void *memchr(const void *_s, int c, size_t n)
{
	const unsigned char * s = _s;
	
	for (; n--; s++)
		if (*s == c)
			return (void*) s;
	
	return NULL;
}
