
#include "memset.h"

void *memset(void * _s, int c, size_t n)
{
	unsigned char* s = _s;
	
	while (n--) *s++ = c;
	
	return _s;
}

