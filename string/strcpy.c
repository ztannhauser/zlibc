
char *strcpy(char *restrict dst, const char *restrict src)
{
	char* retval = dst;
	while ((*dst++ = *src++));
	return retval;
}

