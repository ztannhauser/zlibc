
#include <string/strlen.h>
#include <string/memcpy.h>

#include <stdlib/malloc.h>

#include "strdup.h"

char *strdup(const char * original)
{
	size_t len = strlen(original) + 1;
	
	return memcpy(malloc(len), original, len);
}
