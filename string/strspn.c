
#include <strings/index.h>

#include "strspn.h"

size_t strspn(const char *s, const char *accept)
{
	const char* const t = s;
	
	while (*s && index(accept, *s)) s++;
	
	return s - t;
}
