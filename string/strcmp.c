
#include "strcmp.h"

int strcmp(const char *s1, const char *s2)
{
	int diff;
	
	while (!(diff = *s1 - *s2) && *s1) s1++, s2++;
	
	return diff;
}

