
#include <assert/assert.h>

#include <stdio/stdout.h>
#include <stdio/fflush.h>

#include "stpncpy.h"

char *stpncpy(char *dest, const char *src, size_t n)
{
	while (n-- && (*dest = *src))
		dest++, src++;
	
	return dest;
}

