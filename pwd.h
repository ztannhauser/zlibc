
#ifndef PWD_H
#define PWD_H

// The <pwd.h> header shall define the struct passwd, structure, which shall
// include at least the following members:

// char    *pw_name   User's login name.
// uid_t    pw_uid    Numerical user ID.
// gid_t    pw_gid    Numerical group ID.
// char    *pw_dir    Initial working directory.
// char    *pw_shell  Program to use as shell.

#include <pwd/passwd.h>

// The <pwd.h> header shall define the gid_t, uid_t, and size_t types as
// described in <sys/types.h>.
#include <sys/types/gid_t.h>
#include <sys/types/uid_t.h>
#include <sys/types/size_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.
#include <pwd/endpwent.h>
#include <pwd/getpwent.h>
#include <pwd/getpwnam.h>
#include <pwd/getpwnam_r.h>
#include <pwd/getpwuid.h>
#include <pwd/getpwuid_r.h>
#include <pwd/setpwent.h>


#endif
