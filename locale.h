
#ifndef LOCALE_H
#define LOCALE_H

// Some of the functionality described on this reference page extends the ISO
// C standard. Applications shall define the appropriate feature test  macro
// (see  the  System  Interfaces volume of POSIX.1‐2008, Section 2.2, The
// Compilation Environment) to enable the visibility of these symbols in this
// header.

// The <locale.h> header shall define the lconv structure, which shall include
// at least the following members.   (See  the  definitions  of LC_MONETARY in
// Section 7.3.3, LC_MONETARY and Section 7.3.4, LC_NUMERIC.)

#include <locale/lconv.h>

// The <locale.h> header shall define NULL (as described in <stddef.h>) and at
// least the following as macros:

#include <stddef/NULL.h>

// LC_ALL
// LC_COLLATE
// LC_CTYPE
// LC_MESSAGES
// LC_MONETARY
// LC_NUMERIC
// LC_TIME

// which shall expand to integer constant expressions with distinct values for
// use as the first argument to the setlocale() function.

// Implementations may add additional masks using the form LC_* and an
// uppercase letter.

// The  <locale.h>  header shall contain at least the following macros
// representing bitmasks for use with the newlocale() function for each
// supported locale category: LC_COLLATE_MASK LC_CTYPE_MASK LC_MESSAGES_MASK
// LC_MONETARY_MASK LC_NUMERIC_MASK LC_TIME_MASK

// Implementations may add additional masks using the form LC_*_MASK.

// In addition, a macro to set the bits for all categories set shall be
// defined: LC_ALL_MASK

// The <locale.h> header shall define LC_GLOBAL_LOCALE, a special locale object
// descriptor used by the duplocale()  and  uselocale()  functions.

// The <locale.h> header shall define the locale_t type, representing a locale
// object.

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided for use with ISO C standard
// compilers.

#include <locale/duplocale.h>
#include <locale/freelocale.h>
#include <locale/localeconv.h>
#include <locale/newlocale.h>
#include <locale/setlocale.h>
#include <locale/uselocale.h>



















#endif
