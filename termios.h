
#ifndef TERMIOS_H
#define TERMIOS_H

#include <sys/ioctl.h>

#include <termios/cfgetispeed.h>
#include <termios/cfgetospeed.h>
#include <termios/cfmakeraw.h>
#include <termios/cfsetispeed.h>
#include <termios/cfsetospeed.h>
#include <termios/cfsetspeed.h>
#include <termios/tcdrain.h>
#include <termios/tcflow.h>
#include <termios/tcflush.h>
#include <termios/tcgetattr.h>
#include <termios/tcsendbreak.h>
#include <termios/tcsetattr.h>
#include <termios/winsize.h>

#endif
