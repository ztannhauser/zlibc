

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>

void __real_assert(
	const char* cond,
	const char* file,
	int line)
{
	fflush(stdout);
	fflush(stderr);
	fprintf(stderr,
		"%s: %s:%i: assert(%s) failed.\n",
		program_invocation_name,
		file, line,
		cond);
	abort();
}
















