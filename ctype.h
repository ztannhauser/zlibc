
#ifndef CTYPE_H
#define CTYPE_H

// Some of the functionality described on this reference page extends the ISO
// C standard. Applications shall define the appropriate feature test macro
// (see the System Interfaces volume of POSIX.1‐2008, Section 2.2, The
// Compilation Environment) to enable the visibility of these symbols in this
// header.

// The <ctype.h> header shall define the locale_t type as described in
// <locale.h>, representing a locale object.
#include <locale/locale_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided for use with ISO C standard
// compilers.

#include <ctype/isalnum.h>
#include <ctype/isalnum_l.h>
#include <ctype/isalpha.h>
#include <ctype/isalpha_l.h>
#include <ctype/isascii.h>
#include <ctype/isblank.h>
#include <ctype/isblank_l.h>
#include <ctype/iscntrl.h>
#include <ctype/iscntrl_l.h>
#include <ctype/isdigit.h>
#include <ctype/isdigit_l.h>
#include <ctype/isgraph.h>
#include <ctype/isgraph_l.h>
#include <ctype/islower.h>
#include <ctype/islower_l.h>
#include <ctype/isprint.h>
#include <ctype/isprint_l.h>
#include <ctype/ispunct.h>
#include <ctype/ispunct_l.h>
#include <ctype/isspace.h>
#include <ctype/isspace_l.h>
#include <ctype/isupper.h>
#include <ctype/isupper_l.h>
#include <ctype/isxdigit.h>
#include <ctype/isxdigit_l.h>
#include <ctype/toascii.h>
#include <ctype/tolower.h>
#include <ctype/tolower_l.h>
#include <ctype/toupper.h>
#include <ctype/toupper_l.h>

// The <ctype.h> header shall define the following as macros:

// int   _toupper(int);
// int   _tolower(int);

#if 0
int isalnum(int c); // isdigit() || isalpha()
int isalpha(int c); // isupper() || islower()
int iscntrl(int c); // FF NL CR HT VT || BEL BS
int isdigit(int c); // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
int isgraph(int c); // isalnum() || ispunct()
int islower(int c); // abcdefghijklmnopqrstuvwxyz
int isprint(int c); // ' ' || isgraph()
int ispunct(int c); // !, ", #, %, &, ', (, ), ;, <, =, >, ?, [, "\\", ], *, +, , -, ., /, :, ^
int isspace(int c); // ' ' || FF, NL, CR, HT, VT
int isupper(int c); // ABCDEFGHIJKLMNOPQRSTUVWXYZ
int isxdigit(int c); // isdigit() || ABCDEF || abcdef

int tolower(int c);

int isascii(int c);
int isblank(int c);
#endif




















#endif
