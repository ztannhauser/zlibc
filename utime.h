
// The <utime.h> header shall declare the utimbuf structure:
#include <utime/utimbuf.h>

// The times shall be measured in seconds since the Epoch.

// The <utime.h> header shall define the time_t type as described in
// <sys/types.h>.
#include <sys/types/time_t.h>

// The following shall be declared as a function and may also be defined as a
// macro. A function prototype shall be provided.

#include <utime/utime.h>



