
#include <sys/types/gid_t.h>

// The <grp.h> header shall declare the group structure, which shall include
// the following members:

struct group
{
	char   *gr_name; // The name of the group.
	gid_t   gr_gid;  // Numerical group ID.
	char  **gr_mem;  // Pointer to a null-terminated array of character pointers
	                 // to member names.

};

