
#ifndef GRP_H
#define GRP_H

// The <grp.h> header shall declare the group structure:
#include <grp/group.h>

// The <grp.h> header shall define the gid_t and size_t types as described in
// <sys/types.h>:
#include <sys/types/gid_t.h>
#include <sys/types/size_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided.

#include <grp/endgrent.h>
#include <grp/getgrent.h>
#include <grp/getgrgid.h>
#include <grp/getgrgid_r.h>
#include <grp/getgrnam.h>
#include <grp/getgrnam_r.h>
#include <grp/setgrent.h>

#endif
