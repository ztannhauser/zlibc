
#include <assert/assert.h>

#include "localtime.h"

struct tm *localtime(const time_t * _)
{
	assert(!"TODO");
}

#if 0
#include "tm.h"
#include "gmtime.h"
#include "localtime.h"

// Source: https://sourceware.org/newlib/

#define SECSPERMIN	60L
#define MINSPERHOUR	60L
#define HOURSPERDAY	24L
#define SECSPERHOUR	(SECSPERMIN * MINSPERHOUR)
#define SECSPERDAY	(SECSPERHOUR * HOURSPERDAY)
#define DAYSPERWEEK	7
#define MONSPERYEAR	12

#define YEAR_BASE	1900
#define EPOCH_YEAR      1970
#define EPOCH_WDAY      4
#define EPOCH_YEARS_SINCE_LEAP 2
#define EPOCH_YEARS_SINCE_CENTURY 70
#define EPOCH_YEARS_SINCE_LEAP_CENTURY 370

#define isleap(y) ((((y) % 4) == 0 && ((y) % 100) != 0) || ((y) % 400) == 0)

static const struct __tzinfo_type {
	int __tznorth;
	int __tzyear;
	struct __tzrule_type
	{
		char ch;
		int m; /* Month of year if ch=M */
		int n; /* Week of month if ch=M */
		int d; /* Day of week if ch=M, day of year if ch=J or ch=D */
		int s; /* Time of day in seconds */
		time_t change;
		long offset; /* Match type of _timezone. */
	}  __tzrule[2];
} tzinfo = {};

#define __gettzinfo() (&tzinfo)

static int
__tzcalc_limits (int year)
{
  int days, year_days, years;
  int i, j;
  __tzinfo_type *const tz = __gettzinfo ();

  if (year < EPOCH_YEAR)
    return 0;

  tz->__tzyear = year;

  years = (year - EPOCH_YEAR);

  year_days = years * 365 +
    (years - 1 + EPOCH_YEARS_SINCE_LEAP) / 4 -
    (years - 1 + EPOCH_YEARS_SINCE_CENTURY) / 100 +
    (years - 1 + EPOCH_YEARS_SINCE_LEAP_CENTURY) / 400;

  for (i = 0; i < 2; ++i)
    {
      if (tz->__tzrule[i].ch == 'J')
	{
	  /* The Julian day n (1 <= n <= 365). */
	  days = year_days + tz->__tzrule[i].d +
	    (isleap(year) && tz->__tzrule[i].d >= 60);
	  /* Convert to yday */
	  --days;
	}
      else if (tz->__tzrule[i].ch == 'D')
	days = year_days + tz->__tzrule[i].d;
      else
	{
	  const int yleap = isleap(year);
	  int m_day, m_wday, wday_diff;
	  const int *const ip = __month_lengths[yleap];

	  days = year_days;

	  for (j = 1; j < tz->__tzrule[i].m; ++j)
	    days += ip[j-1];

	  m_wday = (EPOCH_WDAY + days) % DAYSPERWEEK;

	  wday_diff = tz->__tzrule[i].d - m_wday;
	  if (wday_diff < 0)
	    wday_diff += DAYSPERWEEK;
	  m_day = (tz->__tzrule[i].n - 1) * DAYSPERWEEK + wday_diff;

	  while (m_day >= ip[j-1])
	    m_day -= DAYSPERWEEK;

	  days += m_day;
	}

      /* store the change-over time in GMT form by adding offset */
      tz->__tzrule[i].change = (time_t) days * SECSPERDAY +
      tz->__tzrule[i].s + tz->__tzrule[i].offset;
    }

  tz->__tznorth = (tz->__tzrule[0].change < tz->__tzrule[1].change);

  return 1;
}

struct tm* localtime(const time_t *timep)
{
	long offset;
  int hours, mins, secs;
  int year;
  __tzinfo_type *const tz = __gettzinfo ();
  const int *ip;
  struct tm* res;

  res = gmtime(tim_p);

  year = res->tm_year + YEAR_BASE;
  ip = __month_lengths[isleap(year)];

  TZ_LOCK;
  _tzset_unlocked ();
  if (_daylight)
    {
      if (year == tz->__tzyear || __tzcalc_limits (year))
	res->tm_isdst = (tz->__tznorth
	  ? (*tim_p >= tz->__tzrule[0].change
	  && *tim_p < tz->__tzrule[1].change)
	  : (*tim_p >= tz->__tzrule[0].change
	  || *tim_p < tz->__tzrule[1].change));
      else
	res->tm_isdst = -1;
    }
  else
    res->tm_isdst = 0;

  offset = (res->tm_isdst == 1
    ? tz->__tzrule[1].offset
    : tz->__tzrule[0].offset);

  hours = (int) (offset / SECSPERHOUR);
  offset = offset % SECSPERHOUR;

  mins = (int) (offset / SECSPERMIN);
  secs = (int) (offset % SECSPERMIN);

  res->tm_sec -= secs;
  res->tm_min -= mins;
  res->tm_hour -= hours;

  if (res->tm_sec >= SECSPERMIN)
    {
      res->tm_min += 1;
      res->tm_sec -= SECSPERMIN;
    }
  else if (res->tm_sec < 0)
    {
      res->tm_min -= 1;
      res->tm_sec += SECSPERMIN;
    }
  if (res->tm_min >= MINSPERHOUR)
    {
      res->tm_hour += 1;
      res->tm_min -= MINSPERHOUR;
    }
  else if (res->tm_min < 0)
    {
      res->tm_hour -= 1;
      res->tm_min += MINSPERHOUR;
    }
  if (res->tm_hour >= HOURSPERDAY)
    {
      ++res->tm_yday;
      ++res->tm_wday;
      if (res->tm_wday > 6)
	res->tm_wday = 0;
      ++res->tm_mday;
      res->tm_hour -= HOURSPERDAY;
      if (res->tm_mday > ip[res->tm_mon])
	{
	  res->tm_mday -= ip[res->tm_mon];
	  res->tm_mon += 1;
	  if (res->tm_mon == 12)
	    {
	      res->tm_mon = 0;
	      res->tm_year += 1;
	      res->tm_yday = 0;
	    }
	}
    }
  else if (res->tm_hour < 0)
    {
      res->tm_yday -= 1;
      res->tm_wday -= 1;
      if (res->tm_wday < 0)
	res->tm_wday = 6;
      res->tm_mday -= 1;
      res->tm_hour += 24;
      if (res->tm_mday == 0)
	{
	  res->tm_mon -= 1;
	  if (res->tm_mon < 0)
	    {
	      res->tm_mon = 11;
	      res->tm_year -= 1;
	      res->tm_yday = 364 + isleap(res->tm_year + YEAR_BASE);
	    }
	  res->tm_mday = ip[res->tm_mon];
	}
    }
  TZ_UNLOCK;

  return (res);
}

#endif














