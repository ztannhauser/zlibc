
struct itimerspec {
	struct timespec it_interval;	/* timer period */
	struct timespec it_value;		/* timer expiration */
};

