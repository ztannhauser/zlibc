
#include <stddef/size_t.h>

struct tm;

size_t     strftime(char *restrict, size_t, const char *restrict,
          const struct tm *restrict);
