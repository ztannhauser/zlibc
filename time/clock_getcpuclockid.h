
#include <sys/types/pid_t.h>
#include <sys/types/clockid_t.h>

int        clock_getcpuclockid(pid_t, clockid_t *);
