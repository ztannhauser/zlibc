
size_t     strftime_l(char *restrict, size_t, const char *restrict,
              const struct tm *restrict, locale_t);
