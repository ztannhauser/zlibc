
#include <sys/types/timer_t.h>

int        timer_create(clockid_t, struct sigevent *restrict,
              timer_t *restrict);
