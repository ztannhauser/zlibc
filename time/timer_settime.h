
#ifndef TIMER_SETTIME_H
#define TIMER_SETTIME_H

int        timer_settime(timer_t, int, const struct itimerspec *restrict,
	struct itimerspec *restrict);

#endif
