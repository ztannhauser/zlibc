
#ifndef TIMER_GETTIME_H
#define TIMER_GETTIME_H

int        timer_gettime(timer_t, struct itimerspec *);

#endif
