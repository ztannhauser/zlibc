
#ifndef STRUCT_TIMESPEC_H
#define STRUCT_TIMESPEC_H

#include <sys/types/time_t.h>

struct timespec
{
	time_t	tv_sec;		/* seconds */
	long	tv_nsec;	/* nanoseconds */
};

#endif
