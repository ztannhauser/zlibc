
#ifndef STRING_H
#define STRING_H

// The <string.h> header shall define NULL and size_t as described in <stddef.h>
#include <stddef/NULL.h>
#include <stddef/size_t.h>

// The <string.h> header shall define the locale_t type as described in <locale.h>.
#include <locale/locale_t.h>

// The following shall be declared as functions and may also be defined as
// macros. Function prototypes shall be provided for use with ISO C standard
// compilers.

#include <string/memccpy.h>
#include <string/memchr.h>
#include <string/memcmp.h>
#include <string/memcpy.h>
#include <string/memmove.h>
#include <string/memset.h>
#include <string/stpcpy.h>
#include <string/stpncpy.h>
#include <string/strcat.h>
#include <string/strchr.h>
#include <string/strcmp.h>
#include <string/strcoll.h>
#include <string/strcoll_l.h>
#include <string/strcpy.h>
#include <string/strcspn.h>
#include <string/strdup.h>
#include <string/strerror.h>
#include <string/strerror_l.h>
#include <string/strerror_r.h>
#include <string/strlen.h>
#include <string/strncat.h>
#include <string/strncmp.h>
#include <string/strncpy.h>
#include <string/strndup.h>
#include <string/strnlen.h>
#include <string/strpbrk.h>
#include <string/strrchr.h>
#include <string/strsignal.h>
#include <string/strspn.h>
#include <string/strstr.h>
#include <string/strtok.h>
#include <string/strtok_r.h>
#include <string/strxfrm.h>
#include <string/strxfrm_l.h>

// Inclusion of the <string.h> header may also make visible all symbols from
// <stddef.h>.
#include <stddef.h>

#ifdef _GNU_SOURCE

// GNU adds a few more things:
#include <string/strverscmp.h>


#endif

















#endif
