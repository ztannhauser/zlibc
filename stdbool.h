
#ifndef STDBOOL_H
#define STDBOOL_H

// The functionality described on this reference page is aligned with the ISO
// C standard. Any conflict between the  requirements  described here and the
// ISO C standard is unintentional. This volume of POSIX.1‐2008 defers to the
// ISO C standard.

// The <stdbool.h> header shall define the following macros:

// bool
	// Expands to _Bool.
#include <stdbool/bool.h>

// true
	// Expands to the integer constant 1.
#include <stdbool/true.h>

// false
	// Expands to the integer constant 0.
#include <stdbool/false.h>

// __bool_true_false_are_defined
	// Expands to the integer constant 1.

// An application may undefine and then possibly redefine the macros bool,
// true, and false.


#endif
