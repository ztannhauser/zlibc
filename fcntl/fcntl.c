
#include <sys/syscall/SYS_fcntl.h>
#include <sys/syscall/syscall.h>

int fcntl(int fd, int cmd, void* arg)
{
	int retval = syscall(SYS_fcntl, fd, cmd, arg);
	return retval;
}
