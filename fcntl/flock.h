
#ifndef STRUCT_FLOCK_H
#define STRUCT_FLOCK_H

#include <sys/types/off_t.h>
#include <sys/types/pid_t.h>

struct flock
{
	short int l_type;
	short int l_whence;

	off_t l_start;
	off_t l_len;

	pid_t l_pid;
};

#endif
